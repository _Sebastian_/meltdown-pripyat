class CfgPatches
{
    class YK_Radio
    {
        units[]={};
        weapons[]={};
        requiredVersion=1;
        requiredAddons[]={"DZ_Data", "DZ_Radio", "DZ_Gear_Tools", "DZ_Characters", "DZ_Scripts"};
    };
};
class CfgMods
{
    class YK_Radio
    {
        dir="YK_Radio";
        name="YK_Radio";
        credits="Yuki";
        author="Yuki";
        authorID="76561198120473237";
        version="1.0";
        extra=0;
        type="mod";
        inputs="YK_Radio/inputs.xml";
        dependencies[]={"Game", "World", "Mission"};
        class defs
        {
            class imageSets
            {
                files[]={"YK_Radio/gui/imagesets/yuki_krutoy_poc.imageset"};
            };
            class gameScriptModule
            {
                value="";
                files[]={"YK_Radio/scripts/common", "YK_Radio/scripts/3_Game"};
            };
            class worldScriptModule
            {
                value="";
                files[]={"YK_Radio/scripts/common", "YK_Radio/scripts/4_World"};
            };
            class missionScriptModule
            {
                value="";
                files[]={"YK_Radio/scripts/common", "YK_Radio/scripts/5_Mission"};
            };
        };
    };
};
