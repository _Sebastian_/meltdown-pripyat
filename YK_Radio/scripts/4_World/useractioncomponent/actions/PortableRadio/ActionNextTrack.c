class ActionNextTrack: ActionInteractBase
{
	override string GetText()
	{
		return "#next_track";
	}
	
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		YK_Radio mRadio = YK_Radio.Cast(target.GetObject());
        if(mRadio.IsPlaying() && mRadio.IsCassettePlaylist())
		    return true;
        return false;
	}
	
	override void OnExecuteClient( ActionData action_data )
	{
		Object targetObject = YK_Radio.Cast(action_data.m_Target.GetObject());
		YK_Radio mRadio;

		if (targetObject)
		{
			mRadio = YK_Radio.Cast( targetObject );
			if(mRadio && mRadio.IsPlaying() && mRadio.IsCassettePlaylist())
			{
				mRadio.NextTrack();
			}
		}
	}
};