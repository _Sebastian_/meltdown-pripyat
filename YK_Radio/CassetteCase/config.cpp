class CfgPatches
{
    class YK_Cassette_Base
    {
        units[]={"YK_CassetteCase"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class Container_Base;
    class YK_CassetteCase : Container_Base
    {
        scope=2;
        displayName="$STR_Case";
        descriptionShort="$STR_Case_desc";
        model="\dz\gear\tools\cleaning_kit_wood.p3d";
        animClass="Knife";
        rotationFlags=17;
        quantityBar=0;
        weight=580;
        itemSize[]={2, 3};
        itemsCargoSize[]={};
        fragility=0.01;
        attachments[]={"Cassette", "Cassette2", "Cassette3", "Cassette4", "Cassette5", "Cassette6", "Cassette7", "Cassette8", "Cassette9", "Cassette10", "Cassette11", "Cassette12", "Cassette13", "Cassette14", "Cassette15", "Cassette16"};
        hiddenSelections[]={"zbytek"};
        hiddenSelectionsTextures[]={"YK_Radio\CassetteCase\data\cassette_case.paa"};
        hiddenSelectionsMaterials[]={"YK_Radio\CassetteCase\data\cassette_case.rvmat"};
        class DamageSystem
        {
            class GlobalHealth
            {
                class Health
                {
                    hitpoints=100;
                    healthLevels[]={{1, {"YK_Radio\CassetteCase\data\cassette_case.rvmat"}}, {0.7, {"YK_Radio\CassetteCase\data\cassette_case.rvmat"}}, {0.5, {"YK_Radio\CassetteCase\data\cassette_case_damage.rvmat"}}, {0.3, {"YK_Radio\CassetteCase\data\cassette_case_damage.rvmat"}}, {0, {"YK_Radio\CassetteCase\data\cassette_case_destruct.rvmat"}}};
                };
            };
        };
        class MeleeModes
        {
            class Default
            {
                ammo="MeleeLightBlunt";
                range=1;
            };
            class Heavy
            {
                ammo="MeleeLightBlunt_Heavy";
                range=1;
            };
            class Sprint
            {
                ammo="MeleeLightBlunt_Heavy";
                range=2.8;
            };
        };
    };
};
