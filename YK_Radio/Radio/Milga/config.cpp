class CfgPatches
{
    class YK_Radio
    {
        units[]={"YK_Radio"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Gear_Containers", "DZ_Gear_Crafting", "DZ_Gear_Tools", "DZ_Scripts"};
    };
};
class CfgVehicles
{
    class Inventory_Base;
    class YK_Radio : Inventory_Base
    {
        scope=2;
        displayName="Milga Fm-301";
        descriptionShort="$STR_Radio_disc";
        animClass="Knife";
        attachments[]={"BatteryD", "Cassette"};
        fragility=0.01;
        varWetMax=0.35;
        repairableWithKits[]={5, 7};
        repairCosts[]={50, 55};
        model="YK_Radio\Radio\Milga\Milga.p3d";
        weight=1700;
        itemSize[]={2, 2};
        class DamageSystem
        {
            class GlobalHealth
            {
                class Health
                {
                    hitpoints=100;
                    healthLevels[]={{1, {"DZ\gear\radio\data\unitra_wilga.rvmat"}}, {0.7, {"DZ\gear\radio\data\unitra_wilga.rvmat"}}, {0.5, {"DZ\gear\radio\data\unitra_wilga_damage.rvmat"}}, {0.3, {"DZ\gear\radio\data\unitra_wilga_damage.rvmat"}}, {0, {"DZ\gear\radio\data\unitra_wilga_destruct.rvmat"}}};
                };
            };
        };
        class EnergyManager
        {
            hasIcon=1;
            autoSwitchOff=1;
            autoSwitchOffWhenInCargo=0;
            plugType=1;
            energyUsagePerSecond=0.001;
            attachmentAction=1;
            wetnessExposure=0.12;
        };
        class MeleeModes
        {
            class Default
            {
                ammo="MeleeLightBlunt";
                range=1;
            };
            class Heavy
            {
                ammo="MeleeLightBlunt_Heavy";
                range=1;
            };
            class Sprint
            {
                ammo="MeleeLightBlunt_Heavy";
                range=2.8;
            };
        };
    };
};
