class CfgPatches
{
    class YK_Radio4
    {
        units[]={"YK_RadioWalkman"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Gear_Containers", "DZ_Gear_Crafting", "DZ_Gear_Tools", "DZ_Scripts"};
    };
};
class CfgVehicles
{
    class YK_Radio;
    class YK_RadioWalkman : YK_Radio
    {
        scope=2;
        displayName="Sony Walkman WM-3";
        descriptionShort="$STR_Radio4_desc";
        animClass="Knife";
        attachments[]={"BatteryD", "Cassette"};
        inventorySlot[]={"WalkieTalkie"};
        fragility=0.01;
        varWetMax=0.35;
        rotationFlags=16;
        repairableWithKits[]={5, 7};
        repairCosts[]={50, 55};
        model="YK_Radio\Radio\Walkman\Walkman.p3d";
        weight=400;
        itemSize[]={1, 2};
        class DamageSystem
        {
            class GlobalHealth
            {
                class Health
                {
                    hitpoints=100;
                    healthLevels[]={{1, {"YK_Radio\Radio\Walkman\data\Walkman.rvmat"}}, {0.7, {"YK_Radio\Radio\Walkman\data\Walkman.rvmat"}}, {0.5, {"YK_Radio\Radio\Walkman\data\Walkman_damage.rvmat"}}, {0.3, {"YK_Radio\Radio\Walkman\data\Walkman_damage.rvmat"}}, {0, {"YK_Radio\Radio\Walkman\data\Walkman_destruct.rvmat"}}};
                };
            };
        };
        class EnergyManager
        {
            hasIcon=1;
            autoSwitchOff=1;
            autoSwitchOffWhenInCargo=0;
            plugType=1;
            energyUsagePerSecond=0.0001;
            attachmentAction=1;
            wetnessExposure=0.12;
        };
        class MeleeModes
        {
            class Default
            {
                ammo="MeleeLightBlunt";
                range=1;
            };
            class Heavy
            {
                ammo="MeleeLightBlunt_Heavy";
                range=1;
            };
            class Sprint
            {
                ammo="MeleeLightBlunt_Heavy";
                range=2.8;
            };
        };
    };
};
