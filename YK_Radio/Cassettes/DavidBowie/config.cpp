class CfgPatches
{
    class YK_Cassette_DavidBowie
    {
        units[]={"YK_Cassette_Heroes", "YK_Cassette_SpaceOddity", "YK_Cassette_DavidBowie_Collection"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_Heroes : YK_Cassette_Base
    {
        scope=2;
        displayName="David Bowie — Heroes";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\DavidBowie\sounds\Heroes_David_Bowie.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Heroes_David_Bowie";
        };
    };
    class YK_Cassette_SpaceOddity : YK_Cassette_Base
    {
        scope=2;
        displayName="David Bowie - Space Oddity";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\DavidBowie\sounds\Space_Oddity_David_Bowie.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Space_Oddity_David_Bowie";
        };
    };
    class YK_Cassette_DavidBowie_Collection : YK_Cassette_Base
    {
        scope=2;
        displayName="David Bowie Collection";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\DavidBowie\sounds\DavidBowieCollection.paa"};
        class CfgCassette
        {
            isPlaylist=1;
            soundSets[]={"YK_SoundSet_Heroes_David_Bowie", "YK_SoundSet_Space_Oddity_David_Bowie"};
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_Heroes_David_Bowie
    {
        soundShaders[]={"YK_Heroes_David_Bowie_Shader"};
    };
    class YK_SoundSet_Space_Oddity_David_Bowie
    {
        soundShaders[]={"YK_Space_Oddity_David_Bowie_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_Heroes_David_Bowie_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\DavidBowie\sounds\Heroes_David_Bowie", 1}};
    };
    class YK_Space_Oddity_David_Bowie_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\DavidBowie\sounds\Space_Oddity_David_Bowie", 1}};
    };
};
