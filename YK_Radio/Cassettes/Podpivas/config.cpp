class CfgPatches
{
    class YK_Cassette_podpivas
    {
        units[]={"YK_Cassette_Filin"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_Filin : YK_Cassette_Base
    {
        scope=2;
        displayName="PODPIVAS - Filin";
        descriptionShort="";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Podpivas\sounds\Filin.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Filin";
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_Filin
    {
        soundShaders[]={"YK_Filin_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_Filin_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Podpivas\sounds\Filin", 1}};
    };
};
