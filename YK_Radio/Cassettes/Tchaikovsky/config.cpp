class CfgPatches
{
    class YK_Cassette_Tchaikovsky
    {
        units[]={"YK_Cassette_Tchaikovsky_March", "YK_Cassette_Tchaikovsky_Overture", "YK_Cassette_Tchaikovsky_TheChristmasTree", "YK_Cassette_Tchaikovsky_WeSingToday", "YK_Cassette_Tchaikovsky_Collection"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_Tchaikovsky_March : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_March";
        descriptionShort="$STR_March_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Tchaikovsky\sounds\Tchaikovsky_March.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Tchaikovsky_March";
        };
    };
    class YK_Cassette_Tchaikovsky_Overture : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Overture";
        descriptionShort="$STR_Overture_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Tchaikovsky\sounds\Tchaikovsky_Overture.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Tchaikovsky_Overture";
        };
    };
    class YK_Cassette_Tchaikovsky_TheChristmasTree : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_TheChristmasTree";
        descriptionShort="$STR_TheChristmasTree_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Tchaikovsky\sounds\Tchaikovsky_thechristmastree.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Tchaikovsky_TheChristmasTree";
        };
    };
    class YK_Cassette_Tchaikovsky_WeSingToday : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_WeSingToday";
        descriptionShort="$STR_WeSingToday_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Tchaikovsky\sounds\Tchaikovsky_wesingtoday.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Tchaikovsky_WeSingToday";
        };
    };
    class YK_Cassette_Tchaikovsky_Collection : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_TchaikovskyCollection";
        descriptionShort="$STR_CollectionTape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Tchaikovsky\sounds\TchaikovskyCollection.paa"};
        class CfgCassette
        {
            isPlaylist=1;
            soundSets[]={"YK_SoundSet_Tchaikovsky_March", "YK_SoundSet_Tchaikovsky_Overture", "YK_SoundSet_Tchaikovsky_TheChristmasTree", "YK_SoundSet_Tchaikovsky_WeSingToday"};
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_Tchaikovsky_March
    {
        soundShaders[]={"YK_Tchaikovsky_March_Shader"};
    };
    class YK_SoundSet_Tchaikovsky_Overture
    {
        soundShaders[]={"YK_Tchaikovsky_Overture_Shader"};
    };
    class YK_SoundSet_Tchaikovsky_TheChristmasTree
    {
        soundShaders[]={"YK_Tchaikovsky_TheChristmasTree_Shader"};
    };
    class YK_SoundSet_Tchaikovsky_WeSingToday
    {
        soundShaders[]={"YK_Tchaikovsky_WeSingToday_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_Tchaikovsky_March_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Tchaikovsky\sounds\Tchaikovsky_march", 1}};
    };
    class YK_Tchaikovsky_Overture_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Tchaikovsky\sounds\Tchaikovsky_overture", 1}};
    };
    class YK_Tchaikovsky_TheChristmasTree_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Tchaikovsky\sounds\Tchaikovsky_thechristmastree", 1}};
    };
    class YK_Tchaikovsky_WeSingToday_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Tchaikovsky\sounds\Tchaikovsky_wesingtoday", 1}};
    };
};
