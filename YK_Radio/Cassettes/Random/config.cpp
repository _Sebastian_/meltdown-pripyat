class CfgPatches
{
    class YK_Cassette_Random
    {
        units[]={"YK_Cassette_Hentai"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_Hentai : YK_Cassette_Base
    {
        scope=2;
        displayName="S3RL - Hentai";
        descriptionShort="";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Random\sounds\Hentai.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Hentai";
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_Hentai
    {
        soundShaders[]={"YK_Hentai_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_Hentai_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Random\sounds\Hentai", 1}};
    };
};
