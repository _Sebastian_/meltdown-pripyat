class CfgPatches
{
    class YK_Cassette_German
    {
        units[]={"YK_Cassette_WasWollenWirTrinken", "YK_Cassette_WennDieSoldaten", "YK_Cassette_German_Collection"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_WasWollenWirTrinken : YK_Cassette_Base
    {
        scope=2;
        displayName="Was Wollen Wir Trinken";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\German\sounds\WasWollenWirTrinken.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_WasWollenWirTrinken";
        };
    };
    class YK_Cassette_WennDieSoldaten : YK_Cassette_Base
    {
        scope=2;
        displayName="Wenn Die Soldaten";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\German\sounds\WennDieSoldaten.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_WennDieSoldaten";
        };
    };
    class YK_Cassette_German_Collection : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_GermanCollection";
        descriptionShort="$STR_CollectionTape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\German\sounds\GermanCollection.paa"};
        class CfgCassette
        {
            isPlaylist=1;
            soundSets[]={"YK_SoundSet_WasWollenWirTrinken", "YK_SoundSet_WennDieSoldaten"};
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_WasWollenWirTrinken
    {
        soundShaders[]={"YK_WasWollenWirTrinken_Shader"};
    };
    class YK_SoundSet_WennDieSoldaten
    {
        soundShaders[]={"YK_WennDieSoldaten_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_WasWollenWirTrinken_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\German\sounds\WasWollenWirTrinken", 1}};
    };
    class YK_WennDieSoldaten_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\German\sounds\WennDieSoldaten", 1}};
    };
};
