class CfgPatches
{
    class YK_Cassette_Alliance
    {
        units[]={"YK_Cassette_NaZare"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_NaZare : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_NaZare";
        descriptionShort="$STR_NaZare_disc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Alliance\sounds\NaZare.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_NaZare";
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_NaZare
    {
        soundShaders[]={"YK_NaZare_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_NaZare_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Alliance\sounds\NaZare", 1}};
    };
};
