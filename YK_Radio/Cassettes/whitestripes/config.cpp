class CfgPatches
{
    class YK_Cassette_whitestripes
    {
        units[]={"YK_Cassette_SevenNationArmy"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_SevenNationArmy : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_SevenNationArmy";
        descriptionShort="$STR_SevenNationArmy_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\whitestripes\sounds\SevenNationArmy.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_SevenNationArmy";
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_SevenNationArmy
    {
        soundShaders[]={"YK_SevenNationArmy_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_SevenNationArmy_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\whitestripes\sounds\SevenNationArmy", 1}};
    };
};
