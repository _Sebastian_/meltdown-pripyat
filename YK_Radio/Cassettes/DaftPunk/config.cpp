class CfgPatches
{
    class YK_Cassette_DaftPunk
    {
        units[]={"YK_Cassette_GetLuck", "YK_Cassette_HarderBetterFasterStronger", "YK_Cassette_DaftPunk_Collection"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_GetLuck : YK_Cassette_Base
    {
        scope=2;
        displayName="Daft Punk - Get Lucky";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\DaftPunk\sounds\Get_Lucky_Daft_Punk.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Get_Lucky_Daft_Punk";
        };
    };
    class YK_Cassette_HarderBetterFasterStronger : YK_Cassette_Base
    {
        scope=2;
        displayName="Daft Punk - Harder, Better, Faster, Stronger";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\DaftPunk\sounds\Harder_Better_Faster_Stronger_Daft_Punk.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Harder_Better_Faster_Stronger_Daft_Punk";
        };
    };
    class YK_Cassette_DaftPunk_Collection : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_DaftPunkCollection";
        descriptionShort="$STR_CollectionTape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\DaftPunk\sounds\DaftPunkCollection.paa"};
        class CfgCassette
        {
            isPlaylist=1;
            soundSets[]={"YK_SoundSet_Get_Lucky_Daft_Punk", "YK_SoundSet_Harder_Better_Faster_Stronger_Daft_Punk"};
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_Get_Lucky_Daft_Punk
    {
        soundShaders[]={"YK_Get_Lucky_Daft_Punk_Shader"};
    };
    class YK_SoundSet_Harder_Better_Faster_Stronger_Daft_Punk
    {
        soundShaders[]={"YK_Harder_Better_Faster_Stronger_Daft_Punk_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_Get_Lucky_Daft_Punk_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\DaftPunk\sounds\Get_Lucky_Daft_Punk", 1}};
    };
    class YK_Harder_Better_Faster_Stronger_Daft_Punk_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\DaftPunk\sounds\Harder_Better_Faster_Stronger_Daft_Punk", 1}};
    };
};
