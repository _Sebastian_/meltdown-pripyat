class CfgPatches
{
    class YK_Cassette_AtomicHeart
    {
        units[]={"YK_Cassette_KosilYas", "YK_Cassette_TravauDoma", "YK_Cassette_Arlekino", "YK_Cassette_ZvezdnoeLeto", "YK_Cassette_Komarovo", "YK_Cassette_AtomicHeart_Collection"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_KosilYas : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_KosilYas";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\AtomicHeart\sounds\KosilYas.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_KosilYas";
        };
    };
    class YK_Cassette_TravauDoma : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_TravauDoma";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\AtomicHeart\sounds\TravauDoma.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_TravauDoma";
        };
    };
    class YK_Cassette_Arlekino : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Arlekino";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\AtomicHeart\sounds\Arlekino.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Arlekino";
        };
    };
    class YK_Cassette_ZvezdnoeLeto : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_ZvezdnoeLeto";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\AtomicHeart\sounds\ZvezdnoeLeto.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_ZvezdnoeLeto";
        };
    };
    class YK_Cassette_Komarovo : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Komarovo";
        descriptionShort="$STR_Tape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\AtomicHeart\sounds\Komarovo.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Komarovo";
        };
    };
    class YK_Cassette_AtomicHeart_Collection : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_AtomicHeartCollection";
        descriptionShort="$STR_CollectionTape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\AtomicHeart\sounds\AtomicHeartCollection.paa"};
        class CfgCassette
        {
            isPlaylist=1;
            soundSets[]={"YK_SoundSet_KosilYas", "YK_SoundSet_TravauDoma", "YK_SoundSet_Arlekino", "YK_SoundSet_ZvezdnoeLeto", "YK_SoundSet_Komarovo"};
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_KosilYas
    {
        soundShaders[]={"YK_KosilYas_Shader"};
    };
    class YK_SoundSet_TravauDoma
    {
        soundShaders[]={"YK_TravauDoma_Shader"};
    };
    class YK_SoundSet_Arlekino
    {
        soundShaders[]={"YK_Arlekino_Shader"};
    };
    class YK_SoundSet_ZvezdnoeLeto
    {
        soundShaders[]={"YK_ZvezdnoeLeto_Shader"};
    };
    class YK_SoundSet_Komarovo
    {
        soundShaders[]={"YK_Komarovo_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_KosilYas_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\AtomicHeart\sounds\KosilYas", 1}};
    };
    class YK_TravauDoma_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\AtomicHeart\sounds\TravauDoma", 1}};
    };
    class YK_Arlekino_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\AtomicHeart\sounds\Arlekino", 1}};
    };
    class YK_ZvezdnoeLeto_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\AtomicHeart\sounds\ZvezdnoeLeto", 1}};
    };
    class YK_Komarovo_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\AtomicHeart\sounds\Komarovo", 1}};
    };
};
