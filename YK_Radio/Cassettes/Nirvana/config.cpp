class CfgPatches
{
    class YK_Cassette_Nirvana
    {
        units[]={"YK_Cassette_SmellsLikeTeenSpirit"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_SmellsLikeTeenSpirit : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_SmellsLikeTeenSpirit";
        descriptionShort="$STR_SmellsLikeTeenSpirit_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Nirvana\sounds\SmellsLikeTeenSpirit.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_SmellsLikeTeenSpirit";
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_SmellsLikeTeenSpirit
    {
        soundShaders[]={"YK_SmellsLikeTeenSpirit_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_SmellsLikeTeenSpirit_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Nirvana\sounds\SmellsLikeTeenSpirit", 1}};
    };
};
