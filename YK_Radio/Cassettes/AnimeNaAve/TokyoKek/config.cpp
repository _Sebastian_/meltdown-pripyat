class CfgPatches
{
    class YK_Cassette_TK
    {
        units[]={"YK_Cassette_Unravel"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_Unravel : YK_Cassette_Base
    {
        scope=2;
        displayName="TK - Unravel";
        descriptionShort="$STR_Unravel_disc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\AnimeNaAve\TokyoKek\sounds\Unravel.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Unravel";
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_Unravel
    {
        soundShaders[]={"YK_Unravel_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_Unravel_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\AnimeNaAve\TokyoKek\sounds\Unravel", 1}};
    };
};
