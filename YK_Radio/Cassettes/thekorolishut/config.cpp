class CfgPatches
{
    class YK_Cassette_thekorolishut
    {
        units[]={"YK_Cassette_DaiteLydamRomy", "YK_Cassette_DeadAnarhist", "YK_Cassette_DvaDruga", "YK_Cassette_EliMyasoMyzhiki", "YK_Cassette_Joker", "YK_Cassette_KyklaKoldyna", "YK_Cassette_PrignySoSkaly", "YK_Cassette_SevernyiFlot", "YK_Cassette_TheKoroliShut_Collection"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_DaiteLydamRomy : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_DaiteLydamRomy";
        descriptionShort="$STR_DaiteLydamRomy_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\thekorolishut\sounds\DaiteLydamRomy.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_DaiteLydamRomy";
        };
    };
    class YK_Cassette_DeadAnarhist : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_DeadAnarhist";
        descriptionShort="$STR_DeadAnarhist_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\thekorolishut\sounds\DeadAnarhist.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_DeadAnarhist";
        };
    };
    class YK_Cassette_DvaDruga : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_DvaDruga";
        descriptionShort="$STR_DvaDruga_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\thekorolishut\sounds\DvaDruga.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_DvaDruga";
        };
    };
    class YK_Cassette_EliMyasoMyzhiki : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_EliMyasoMyzhiki";
        descriptionShort="$STR_EliMyasoMyzhiki_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\thekorolishut\sounds\EliMyasoMyzhiki.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_EliMyasoMyzhiki";
        };
    };
    class YK_Cassette_Joker : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Joker";
        descriptionShort="$STR_Joker_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\thekorolishut\sounds\Joker.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Joker";
        };
    };
    class YK_Cassette_KyklaKoldyna : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_KyklaKoldyna";
        descriptionShort="$STR_KyklaKoldyna_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\thekorolishut\sounds\KyklaKoldyna.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_KyklaKoldyna";
        };
    };
    class YK_Cassette_PrignySoSkaly : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_PrignySoSkaly";
        descriptionShort="$STR_PrignySoSkaly_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\thekorolishut\sounds\PrignySoSkaly.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_PrignySoSkaly";
        };
    };
    class YK_Cassette_SevernyiFlot : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_SevernyiFlot";
        descriptionShort="$STR_SevernyiFlot_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\thekorolishut\sounds\SevernyiFlot.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_SevernyiFlot";
        };
    };
    class YK_Cassette_TheKoroliShut_Collection : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_TheKoroliShutCollection";
        descriptionShort="$STR_CollectionTape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\thekorolishut\sounds\TheKoroliShutCollection.paa"};
        class CfgCassette
        {
            isPlaylist=1;
            soundSets[]={"YK_SoundSet_DaiteLydamRomy", "YK_SoundSet_DeadAnarhist", "YK_SoundSet_DvaDruga", "YK_SoundSet_EliMyasoMyzhiki", "YK_SoundSet_Joker", "YK_SoundSet_KyklaKoldyna", "YK_SoundSet_PrignySoSkaly", "YK_SoundSet_SevernyiFlot"};
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_DaiteLydamRomy
    {
        soundShaders[]={"YK_DaiteLydamRomy_Shader"};
    };
    class YK_SoundSet_DeadAnarhist
    {
        soundShaders[]={"YK_DeadAnarhist_Shader"};
    };
    class YK_SoundSet_DvaDruga
    {
        soundShaders[]={"YK_DvaDruga_Shader"};
    };
    class YK_SoundSet_EliMyasoMyzhiki
    {
        soundShaders[]={"YK_EliMyasoMyzhiki_Shader"};
    };
    class YK_SoundSet_Joker
    {
        soundShaders[]={"YK_Joker_Shader"};
    };
    class YK_SoundSet_KyklaKoldyna
    {
        soundShaders[]={"YK_KyklaKoldyna_Shader"};
    };
    class YK_SoundSet_PrignySoSkaly
    {
        soundShaders[]={"YK_PrignySoSkaly_Shader"};
    };
    class YK_SoundSet_SevernyiFlot
    {
        soundShaders[]={"YK_SevernyiFlot_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_DaiteLydamRomy_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\thekorolishut\sounds\DaiteLydamRomy", 1}};
    };
    class YK_DeadAnarhist_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\thekorolishut\sounds\DeadAnarhist", 1}};
    };
    class YK_DvaDruga_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\thekorolishut\sounds\DvaDruga", 1}};
    };
    class YK_EliMyasoMyzhiki_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\thekorolishut\sounds\EliMyasoMyzhiki", 1}};
    };
    class YK_Joker_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\thekorolishut\sounds\Joker", 1}};
    };
    class YK_KyklaKoldyna_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\thekorolishut\sounds\KyklaKoldyna", 1}};
    };
    class YK_PrignySoSkaly_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\thekorolishut\sounds\PrignySoSkaly", 1}};
    };
    class YK_SevernyiFlot_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\thekorolishut\sounds\SevernyiFlot", 1}};
    };
};
