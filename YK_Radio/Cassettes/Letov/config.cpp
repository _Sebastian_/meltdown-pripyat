class CfgPatches
{
    class YK_Cassette_Letov
    {
        units[]={"YK_Cassette_Dyrachok", "YK_Cassette_Motylek", "YK_Cassette_Oborona", "YK_Cassette_Vesna", "YK_Cassette_Zoopark", "YK_Cassette_Letov_Collection"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_Dyrachok : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Dyrachok";
        descriptionShort="$STR_Dyrachok_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Letov\sounds\Dyrachok.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Dyrachok";
        };
    };
    class YK_Cassette_Motylek : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Motylek";
        descriptionShort="$STR_Motylek_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Letov\sounds\Motylek.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Motylek";
        };
    };
    class YK_Cassette_Oborona : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Oborona";
        descriptionShort="$STR_Oborona_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Letov\sounds\Oborona.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Oborona";
        };
    };
    class YK_Cassette_Vesna : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Vesna";
        descriptionShort="$STR_Vesna_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Letov\sounds\Vesna.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Vesna";
        };
    };
    class YK_Cassette_Zoopark : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Zoopark";
        descriptionShort="$STR_Zoopark_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Letov\sounds\Zoopark.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Zoopark";
        };
    };
    class YK_Cassette_Letov_Collection : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_LetovCollection";
        descriptionShort="$STR_CollectionTape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Letov\sounds\LetovCollection.paa"};
        class CfgCassette
        {
            isPlaylist=1;
            soundSets[]={"YK_SoundSet_Dyrachok", "YK_SoundSet_Motylek", "YK_SoundSet_Oborona", "YK_SoundSet_Vesna", "YK_SoundSet_Zoopark"};
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_Motylek
    {
        soundShaders[]={"YK_Motylek_Shader"};
    };
    class YK_SoundSet_Dyrachok
    {
        soundShaders[]={"YK_Dyrachok_Shader"};
    };
    class YK_SoundSet_Oborona
    {
        soundShaders[]={"YK_Oborona_Shader"};
    };
    class YK_SoundSet_Vesna
    {
        soundShaders[]={"YK_Vesna_Shader"};
    };
    class YK_SoundSet_Zoopark
    {
        soundShaders[]={"YK_Zoopark_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_Dyrachok_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Letov\sounds\Dyrachok", 1}};
    };
    class YK_Motylek_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Letov\sounds\Motylek", 1}};
    };
    class YK_Oborona_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Letov\sounds\Oborona", 1}};
    };
    class YK_Vesna_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Letov\sounds\Vesna", 1}};
    };
    class YK_Zoopark_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Letov\sounds\Zoopark", 1}};
    };
};
