class CfgPatches
{
    class YK_Cassette_MolchatDoma
    {
        units[]={"YK_Cassette_Sudno", "YK_Cassette_Discoteque", "YK_Cassette_Kletka", "YK_Cassette_Lydi", "YK_Cassette_Volny", "YK_Cassette_MolchatDoma_Collection"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_Sudno : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Sudno";
        descriptionShort="$STR_Sudno_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\MolchatDoma\sounds\Sudno.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Sudno";
        };
    };
    class YK_Cassette_Discoteque : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Discoteque";
        descriptionShort="$STR_Discoteque_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\MolchatDoma\sounds\Discoteque.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Discoteque";
        };
    };
    class YK_Cassette_Kletka : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Kletka";
        descriptionShort="$STR_Kletka_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\MolchatDoma\sounds\Kletka.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Kletka";
        };
    };
    class YK_Cassette_Lydi : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_LudiNadoeli";
        descriptionShort="STR_LudiNadoeli_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\MolchatDoma\sounds\Lydi.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Lydi";
        };
    };
    class YK_Cassette_Volny : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Volny";
        descriptionShort="$STR_Volny_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\MolchatDoma\sounds\Volny.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Volny";
        };
    };
    class YK_Cassette_MolchatDoma_Collection : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_MolchatDomaCollection";
        descriptionShort="$STR_CollectionTape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\MolchatDoma\sounds\MolchatDomaCollection.paa"};
        class CfgCassette
        {
            isPlaylist=1;
            soundSets[]={"YK_SoundSet_Sudno", "YK_SoundSet_Discoteque", "YK_SoundSet_Kletka", "YK_SoundSet_Lydi", "YK_SoundSet_Volny"};
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_Sudno
    {
        soundShaders[]={"YK_Sudno_Shader"};
    };
    class YK_SoundSet_Discoteque
    {
        soundShaders[]={"YK_Discoteque_Shader"};
    };
    class YK_SoundSet_Kletka
    {
        soundShaders[]={"YK_Kletka_Shader"};
    };
    class YK_SoundSet_Lydi
    {
        soundShaders[]={"YK_Lydi_Shader"};
    };
    class YK_SoundSet_Volny
    {
        soundShaders[]={"YK_Volny_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_Sudno_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\MolchatDoma\sounds\Sudno", 0.5}};
    };
    class YK_Discoteque_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\MolchatDoma\sounds\Discoteque", 0.5}};
    };
    class YK_Kletka_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\MolchatDoma\sounds\Kletka", 0.5}};
    };
    class YK_Lydi_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\MolchatDoma\sounds\Lydi", 0.5}};
    };
    class YK_Volny_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\MolchatDoma\sounds\Volny", 0.5}};
    };
};
