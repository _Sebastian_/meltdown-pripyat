class CfgPatches
{
    class YK_Cassette_Base
    {
        units[]={};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class Inventory_Base;
    class YK_Cassette_Base2 : Inventory_Base
    {
        displayName="$STR_Cassette";
        descriptionShort="$STR_Cassette_disc";
        model="YK_Radio\Cassettes\Clear2\CassetteNew.p3d";
        rotationFlags=16;
        weight=100;
        itemSize[]={2, 1};
        fragility=0.01;
        varWetMax=0.5;
        repairableWithKits[]={5, 7};
        repairCosts[]={30, 25};
        inventorySlot[]={"Cassette"};
        hiddenSelections[]={"camo"};
        class DamageSystem
        {
            class GlobalHealth
            {
                class Health
                {
                    hitpoints=100;
                    healthLevels[]={{1, {"YK_Radio\Cassettes\Clear2\data\cassette.rvmat"}}, {0.7, {"YK_Radio\Cassettes\Clear2\data\cassette.rvmat"}}, {0.5, {"YK_Radio\Cassettes\Clear\data\cassette_damage.rvmat"}}, {0.3, {"YK_Radio\Cassettes\Clear\data\cassette_damage.rvmat"}}, {0, {"YK_Radio\Cassettes\Clear\data\cassette_destruct.rvmat"}}};
                };
            };
        };
        class MeleeModes
        {
            class Default
            {
                ammo="MeleeLightBlunt";
                range=1;
            };
            class Heavy
            {
                ammo="MeleeLightBlunt_Heavy";
                range=1;
            };
            class Sprint
            {
                ammo="MeleeLightBlunt_Heavy";
                range=2.8;
            };
        };
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base
    {
        samples[]={};
        frequency=1;
        range=100;
        volume=1;
    };
};
class CfgNonAIVehicles
{
    class ProxyAttachment;
    class Proxygavno : ProxyAttachment
    {
        scope=0;
        inventorySlot="Cassette";
        model="YK_Radio\Cassettes\Clear2\proxy\gavno.p3d";
    };
};
