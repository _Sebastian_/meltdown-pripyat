class CfgPatches
{
    class YK_Cassette_Gachimuchi
    {
        units[]={"YK_Cassette_Alert", "YK_Cassette_TokyoGhoul", "YK_Cassette_Gachimuchi_Collection"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_Alert : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_Alert";
        descriptionShort="$STR_Alert_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Gachimuchi\sounds\Alert.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_Alert";
        };
    };
    class YK_Cassette_TokyoGhoul : YK_Cassette_Base
    {
        scope=2;
        displayName="TK - Unravel";
        descriptionShort="$STR_Unravel_disc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\AnimeNaAve\TokyoKek\sounds\Unravel.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_TokyoGhoul";
        };
    };
    class YK_Cassette_Gachimuchi_Collection : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_GachimuchiCollection";
        descriptionShort="$STR_CollectionTape";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Gachimuchi\sounds\GachimuchiCollection.paa"};
        class CfgCassette
        {
            isPlaylist=1;
            soundSets[]={"YK_SoundSet_Alert", "YK_SoundSet_TokyoGhoul"};
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_Alert
    {
        soundShaders[]={"YK_Alert_Shader"};
    };
    class YK_SoundSet_TokyoGhoul
    {
        soundShaders[]={"YK_TokyoGhoul_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_Alert_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Gachimuchi\sounds\Alert", 1}};
    };
    class YK_TokyoGhoul_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Gachimuchi\sounds\TokyoGhoul", 1}};
    };
};
