class CfgPatches
{
    class YK_Cassette_Tatu
    {
        units[]={"YK_Cassette_NasNeDogoniat"};
        weapons[]={};
        requiredVersion=0.1;
        requiredAddons[]={"DZ_Data"};
    };
};
class CfgVehicles
{
    class YK_Cassette_Base;
    class YK_Cassette_NasNeDogoniat : YK_Cassette_Base
    {
        scope=2;
        displayName="$STR_NasNeDogoniat";
        descriptionShort="$STR_NasNeDogoniat_desc";
        hiddenSelectionsTextures[]={"YK_Radio\Cassettes\Tatu\sounds\NasNeDogoniat.paa"};
        class CfgCassette
        {
            soundSet="YK_SoundSet_NasNeDogoniat";
        };
    };
};
class CfgSoundSets
{
    class Mods_SoundSet_Base;
    class YK_SoundSet_NasNeDogoniat
    {
        soundShaders[]={"YK_NasNeDogoniat_Shader"};
    };
};
class CfgSoundShaders
{
    class YK_Cassette_SoundShader_Base;
    class YK_NasNeDogoniat_Shader : YK_Cassette_SoundShader_Base
    {
        samples[]={{"YK_Radio\Cassettes\Tatu\sounds\NasNeDogoniat", 1}};
    };
};
