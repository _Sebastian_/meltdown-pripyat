class CfgPatches
{
	class DZM_Pack
	{
		requiredAddons[]=
		{
			"DZ_Data",
			"DZ_Characters",
			"DZ_Characters_Backpacks",
			"DZ_Gear_Containers",
			"DZ_Gear_Consumables",
			"DZ_Gear_Tools",
			"DZ_Gear_Cooking",
			"DZ_Gear_Food",
			"DZ_Gear_Navigation",
			"DZ_Gear_Drinks",
			"DZ_Gear_Camping",
			"DZ_Gear_Optics",
			"DZ_Gear_Crafting",
			"DZ_Weapons_Firearms",
			"DZ_Weapons_Firearms_CZ527",
			"DZ_Weapons_Projectiles",
			"DZ_Characters_Vests",
			"DZ_Radio",
			"SyberiaScripts",
			// "SyberiaGeigerCounter",
			"Windstride_Clothing",
			"Canvas_Backpack",
			"LoftDmodGR_Core",
			"LoftDmodGR_scripts",
			"Loftd_backpacks",
			"Munghardshikingbag"
		};
	};
};

class CfgMods
{
	class DZM_Pack
	{
		type = "mod";

		class defs
		{
			class gameScriptModule
			{
				value = "";
				files[] = { "DZM-Pack/scripts/3_game" };
			};

			class worldScriptModule
			{
				value = "";
				files[] = { "DZM-Pack/scripts/4_world" };
			};

			class missionScriptModule
			{
				value = "";
				files[] = { "DZM-Pack/scripts/5_mission" };
			};
		};
	};
};

//search for loot override
class CfgSFLProxies
{
    proxies[] = {
    	"static_kitchen_desk_a",
        "static_kitchen_desk_b",
        "static_kitchen_unit_a_dz",
        "static_skrin_bar",
        "static_bar_shelves_dz",
        "static_fridge",
        "static_fridge335",
        "static_kitchenstove_elec",
        "static_vending_machine",

        "static_almara",
        "static_case_a",
        "static_case_bedroom_b",
        "static_case_bedroom_b652_prx",
        "static_case_cans_b",
        "static_case_chest_dz",
        "static_class_case_a_closed",
        "static_dhangar_brownskrin",
        "static_dhangar_brownskrin_old",
        "static_washing_machine",
        
        "static_case_sink_a",
        "static_lab_sink",
        "static_lekarnicka",
        "static_locker_closed_v1",
        "static_locker_closed_v2",
        "static_locker_closed_v3",
        "static_locker_closed_blue_v1",
        "static_locker_closed_blue_v2",
        "static_locker_closed_blue_v3",
        "static_teacher_desk",
        "static_lab_teacher_bench",
        "static_classroom_case_a",
        "static_lab_bench",
        "static_truck_01_aban_1_lod1parts_fire",
        "static_class_case_b_closed",
        "static_metalcase_01",
        "static_library_a",
        "static_office_desk",
        "static_office_table_a",
        "static_bag_dz",
        "static_box_c_multi",
        "static_box_c",
        "static_library_a_open",
        "static_conference_table_a",
        "static_table_drawer",
        "static_desk_office",
        "static_case_bedroom_a",
        "static_table_umakart",
        "static_tent_boxwooden",
        "static_misc_boxwooden"
    };
};

class cfgLiquidDefinitions
{
	class Vodka
	{
		class Nutrition
		{
			fullnessIndex=1;
			energy=221;
			water=60;
			nutritionalIndex=75;
			toxicity=0.050000001;
			digestibility=2;
		};
	};

    class Ketchup
    {
        class Nutrition
        {
            fullnessIndex=5;
            energy=112;
            water=69/4;
            nutritionalIndex=75;
            toxicity=-0.0099999998;
			digestibility=2;
        };
    };
};

class CfgWeapons
{
    class RifleCore;
    class Rifle_Base : RifleCore
    {
        inventorySlot[]+={"weaponsbag", "wbelt_back", "wbelt_back_right", "wbelt_front", "wbelt_right"};
    };
};

class CfgMagazines
{
    class DefaultMagazine;
    class Magazine_Base;

    class Mag_CZ527_5rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine3", "pistolmagazine", "pistolmagazine1", "pistolmagazine2"};
    };
    class Mag_CZ527_10rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine3", "pistolmagazine", "pistolmagazine1", "pistolmagazine2"};
    };
    class Mag_M14_10Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine4", "magazine5", "magazine6", "magazine8"};
    };
    class Mag_M14_20Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine4", "magazine5", "magazine6", "magazine8"};
    };
    class Mag_MP5_15Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine4", "magazine5", "magazine6", "magazine8"};
    };
    class Mag_MP5_30Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine4", "magazine5", "magazine6", "magazine8"};
    };
    class Mag_Ruger1022_15Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine4", "magazine5", "magazine6", "magazine8"};
    };
    class Mag_Ruger1022_30Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine4", "magazine5", "magazine6", "magazine8"};
    };
    class Mag_SSG82_5rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine3", "pistolmagazine", "pistolmagazine1", "pistolmagazine2"};
    };
    class Mag_SV98_10Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine4", "magazine5", "magazine6", "magazine8"};
    };
    class Mag_Saiga_5Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine4", "magazine5", "magazine6", "magazine8"};
    };
    class Mag_Saiga_8Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine4", "magazine5", "magazine6", "magazine8"};
    };
    class Mag_Scout_5Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine3", "pistolmagazine", "pistolmagazine1", "pistolmagazine2"};
    };
    class Mag_UMP_25Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine4", "magazine5", "magazine6", "magazine8"};
    };
    class Mag_Vikhr_30Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine7", "magazine4", "magazine5", "magazine6", "magazine8"};
    };

    class Mag_lebedev_PL15_16Rnd : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine3", "pistolmagazine", "pistolmagazine1", "pistolmagazine2"};
    };
    class Mag_cs5_10Rnd_mung : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine3", "pistolmagazine", "pistolmagazine1", "pistolmagazine2"};
    };
    class Mag_gewehr43_10Rnd_mung : Magazine_Base
    {
        inventorySlot[]+={"magazine", "magazine2", "magazine3", "pistolmagazine", "pistolmagazine1", "pistolmagazine2"};
    };
}; 

class CfgAmmo
{
    class ShotgunCore;
    class DefaultAmmo;
    class BulletCore;
    class Bullet_Base: BulletCore
    {
        damageBarrel=10;
        damageBarrelDestroyed=10;
    };
    class Shotgun_Base: ShotgunCore
    {
        damageBarrel=10;
        damageBarrelDestroyed=10;
    };
    class Bullet_12GaugePellets: Shotgun_Base
    {
        damageBarrel=60;
        damageBarrelDestroyed=60;
    };
    class Bullet_12GaugeSlug: Bullet_Base
    {
        damageBarrel=60;
        damageBarrelDestroyed=60;
    };
    class Bullet_12GaugeRubberSlug: Bullet_12GaugeSlug
    {
        damageBarrel=48;
        damageBarrelDestroyed=48;
    };
    class Bullet_12GaugeBeanbag: Bullet_12GaugeSlug
    {
        damageBarrel=50;
        damageBarrelDestroyed=50;
    };
    class Bullet_556x45 : Bullet_Base
    {
        damageBarrel=25;
        damageBarrelDestroyed=25;
    };
    class Bullet_545x39 : Bullet_Base
    {
        damageBarrel=21;
        damageBarrelDestroyed=21;
    };
    class Bullet_762x54 : Bullet_Base
    {
        damageBarrel=50;
        damageBarrelDestroyed=50;
    };
    class Bullet_308Win : Bullet_Base
    {
        damageBarrel=50;
        damageBarrelDestroyed=50;
    };
    class Bullet_762x39 : Bullet_Base
    {
        damageBarrel=50;
        damageBarrelDestroyed=50;
    };
    class Bullet_45ACP : Bullet_Base
    {
        damageBarrel=19;
        damageBarrelDestroyed=19;
    };
    class Bullet_357 : Bullet_Base
    {
        damageBarrel=19;
        damageBarrelDestroyed=19;
    };
    class Bullet_9x19 : Bullet_Base
    {
        damageBarrel=17;
        damageBarrelDestroyed=17;
    };
    class Bullet_380 : Bullet_Base
    {
        damageBarrel=15;
        damageBarrelDestroyed=15;
    };
    class Bullet_22 : Bullet_Base
    {
        damageBarrel=12;
        damageBarrelDestroyed=12;
    };
    class Bullet_9x39AP : Bullet_Base
    {
        damageBarrel=25;
        damageBarrelDestroyed=25;
    };
    class Bullet_9x39 : Bullet_Base
    {
        damageBarrel=25;
        damageBarrelDestroyed=25;
    };
    class Bolt_Base: BulletCore
    {
        damageBarrel=30;
        damageBarrelDestroyed=30;
    };
    class Bullet_40mm_Base: Bullet_Base
    {
        damageBarrel=50;
        damageBarrelDestroyed=50;
    };

//Expansion
    class Bullet_Expansion_8mm : Bullet_Base
    {
        damageBarrel=60;
        damageBarrelDestroyed=60;
    };

};


class CfgSlots
{
    class Slot_wallet
    {
        name="Wallet";
        displayName="Wallet";
        ghostIcon="";
    };
};

class CfgVehicles
{
	class Inventory_Base;
	class Container_Base;
	class Clothing_Base;
	class Clothing: Clothing_Base
	{
	};
	class Edible_Base: Inventory_Base
	{
		// inventorySlot[] =
		// {
		// 	"DirectCookingA",
		// 	"DirectCookingB",
		// 	"DirectCookingC"
		// };
	};

	class ItemCompass: Inventory_Base
	{
	};

	class OrienteeringCompass: ItemCompass
	{
		inventorySlot[]=
		{
			"Compass"
		};
	};

//fixes for other mod overrides
	class Paper : Inventory_Base
	{
		canBeSplit = 0;
		varQuantityInit = 0;
		varQuantityMin = 0;
		varQuantityMax = 0;
		varStackMax = 0;
		varQuantityDestroyOnMin = 0;
	};
	class BoneHook : Inventory_Base
	{
		canBeSplit = 0;
		varQuantityInit = 0;
		varQuantityMin = 0;
		varQuantityMax = 0;
		varStackMax = 0;
		varQuantityDestroyOnMin = 0;
	};

//BAYONET
	class AK_Bayonet: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1500;
				};
			};
		};
	};
	class M9A1_Bayonet: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1500;
				};
			};
		};
	};
	class Mosin_Bayonet: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1500;
				};
			};
		};
	};
	class SKS_Bayonet: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1500;
				};
			};
		};
	};

//BLADES
	class WoodAxe: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2500;
				};
			};
		};
	};
	class Hatchet: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1500;
				};
			};
		};
	};
	class FirefighterAxe: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2500;
				};
			};
		};
	};
	class Cleaver: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1400;
				};
			};
		};
	};
	class CombatKnife: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1500;
				};
			};
		};
	};
	class HuntingKnife: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1300;
				};
			};
		};
	};
	class Machete: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};
	class Pitchfork: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};
	class Pickaxe: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2500;
				};
			};
		};
	};
	class Sword: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};
	class KukriKnife: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1500;
				};
			};
		};
	};
	class FangeKnife: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1300;
				};
			};
		};
	};
	class CrudeMachete: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};
	class OrientalMachete: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};

//BLUNT
	class BaseballBat: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2500;
				};
			};
		};
	};
	class NailedBaseballBat: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2500;
				};
			};
		};
	};
	class BarbedBaseballBat: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2500;
				};
			};
		};
	};
	class PipeWrench: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 4000;
				};
			};
		};
	};
	class BrassKnuckles_ColorBase: Inventory_Base{};
	class BrassKnuckles_Dull: BrassKnuckles_ColorBase
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 4000;
				};
			};
		};
	};
	class BrassKnuckles_Shiny: BrassKnuckles_ColorBase
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 4000;
				};
			};
		};
	};


//TOOLS
	class Torch: Inventory_Base
	{
		burnTimePerRag=240;
		burnTimePerFullLardDose=1800;
		burnTimePerFullFuelDose=1200;
	};

	class Roadflare: Inventory_Base
	{
		class EnergyManager
		{
			energyAtSpawn=2400;
		};
	};

	class CanOpener: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 200;
				};
			};
		};
	};
	class Pliers: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1000;
				};
			};
		};
	};
	class Lockpick: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1250;
				};
			};
		};
	};
	class Shovel: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};
	class FieldShovel: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2500;
				};
			};
		};
	};
	class Crowbar: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 5000;
				};
			};
		};
	};
	class Hammer: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1000;
				};
			};
		};
	};
	class MeatTenderizer: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 500;
				};
			};
		};
	};
	class Wrench: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1000;
				};
			};
		};
	};
	class LugWrench: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 4000;
				};
			};
		};
	};
	class Pipe: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 5000;
				};
			};
		};
	};
	class Screwdriver: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1000;
				};
			};
		};
	};
	class Sickle: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1000;
				};
			};
		};
	};
	class Hacksaw: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1000;
				};
			};
		};
	};
	class KitchenKnife: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 850;
				};
			};
		};
	};
	class SteakKnife: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 650;
				};
			};
		};
	};
	class Mace: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 3000;
				};
			};
		};
	};
	class FarmingHoe: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};
	class SledgeHammer: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 3000;
				};
			};
		};
	};
	class Iceaxe : Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};
	class HandSaw: Inventory_Base
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1000;
				};
			};
		};
	};

	class NioshFaceMask: Clothing
	{
		radiationProtection = 0.5;
		// class Protection
		// {
		//     nuclear = 0.75;
		// };
	}
	class SurgicalMask: Clothing
	{
		radiationProtection = 0.375;
		// class Protection
		// {
		//     nuclear = 0.5;
		// };
	}
	class GasMask: Clothing
	{
	    radiationProtection = 0.99;
	};
	class GasMask_Filter: Inventory_Base
	{
	    radiationProtection = 0.99;
	};
	class GasMask_Filter_Improvised: Inventory_Base
	{
	    radiationProtection = 0.95;
	};

	class PlateCarrierPouches: Container_Base
	{
		itemSize[]={3,3};//6,4
		itemsCargoSize[]={3,3};//6,4
	};

	class AmmoBox: Container_Base
	{
		itemSize[]={4,3};
	};

// HEAD
	class NBCHoodBase: Clothing
	{
		itemSize[]={2,1};
		radiationProtection = 0.99;
	};
	class WitchHood_ColorBase: Clothing
	{
		varWetMax=0.49000001;
	};
	class WitchHoodCoif_ColorBase: Clothing
	{
		varWetMax=0.49000001;
	};
	class WitchHat: Clothing
	{
		varWetMax=0.49000001;
	};
	class Chainmail_Coif: Clothing
	{
		varWetMax=0.49000001;
	};

// TOPS
	class DownJacket_ColorBase : Clothing
	{
		itemsCargoSize[] = {5,1};
	};

	class Sweater_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class Shirt_ColorBase: Clothing
	{
		itemsCargoSize[]={3,1};
	};
	class TShirt_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class Hoodie_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class TacticalShirt_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class HikingJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class Raincoat_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		weight=800;
		itemSize[]={2,1};
		itemsCargoSize[]={2,1};
		radiationProtection = 0.95;
		// class Protection
		// {
		//     nuclear = 0.9;
		// };
	};
	class TorsoCover_Improvised: Clothing
	{
		itemsCargoSize[]={1,1};
	};
	class M65Jacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class TTsKOJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={9,1};
	};
	class GorkaEJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class RidersJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class WoolCoat_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={4,1};
	};
	class TrackSuitJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class PoliceJacket: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class PoliceJacketOrel: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class ParamedicJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={4,1};
	};
	class FirefighterJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
		// radiationProtection = 0.19;
		// class Protection
		// {
		//     nuclear = 0.9;
		// };
	};
	class PrisonUniformJacket: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class MiniDress_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class QuiltedJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={6,1};
	};
	class BomberJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class LeatherJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class HuntingJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class MedicalScrubsShirt_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class LabCoat: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class NurseDress_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class USMCJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Blouse_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class NBCJacketBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemSize[]={2,1};
		itemsCargoSize[]={0,0};
		radiationProtection = 0.99;
	};
	class DenimJacket: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class TelnyashkaShirt: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class ChernarusSportShirt: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class JumpsuitJacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class BDUJacket: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class ManSuit_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class WomanSuit_ColorBase: Clothing
	{
		itemsCargoSize[]={4,1};
	};
	class LeatherShirt_ColorBase: Clothing
	{
		itemsCargoSize[]={1,1};
	};
	class Chainmail: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		varWetMax=0.49000001;
		itemsCargoSize[]={0,0};
		quickBarBonus=0;
	};


// HANDS
	class NBCGloves_ColorBase: Clothing
	{
		itemSize[]={2,1};
		radiationProtection = 0.99;
	};
	class SurgicalGloves_ColorBase: Clothing
	{
		radiationProtection = 0.95;
		// class Protection
		// {
		//     nuclear = 0.9;
		// };
	};

// LEGS
	class Jeans_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
	};
	class CargoPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class TTSKOPants: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={7,1};
	};
	class HunterPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class CanvasPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
	};
	class CanvasPantsMidi_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
		notOutside=1;
	};
	class TrackSuitPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={4,1};
	};
	class GorkaPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={7,1};
	};
	class PolicePants: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={5,1};
	};
	class PolicePantsOrel: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={7,1};
	};
	class ParamedicPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={7,1};
	};
	class FirefightersPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={7,1};
		// radiationProtection = 0.09;
		// class Protection
		// {
		//     nuclear = 0.9;
		// };
	};
	class PrisonUniformPants: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={4,1};
	};
	class LeatherPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={2,1};
	};
	class MedicalScrubsPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={5,1};
	};
	class USMCPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class SlacksPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={5,1};
	};
	class BDUPants: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class NBCPantsBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemSize[]={2,1};
		itemsCargoSize[]={0,0};
		quickBarBonus=0;
		radiationProtection = 0.99;
	};
	class Breeches_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
		notOutside=1;
	};
	class ShortJeans_ColorBase: Clothing
	{
		itemsCargoSize[]={6,1};
		notOutside=1;
	};
	class Skirt_ColorBase: Clothing
	{
		itemsCargoSize[]={3,1};
		notOutside=1;
	};
	class JumpsuitPants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={7,1};
	};
	class LegsCover_Improvised: Clothing
	{
		itemsCargoSize[]={1,1};
		notOutside=1;
	};
	class Chainmail_Leggings: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		varWetMax=0.49000001;
		itemsCargoSize[]={0,0};
		quickBarBonus=0;
	};


// FEET
	class AthleticShoes_ColorBase: Clothing
	{
		itemSize[]={3,1};
	};
	class HikingBoots_ColorBase: Clothing
	{
		itemSize[]={3,2};
	};
	class HikingBootsLow_ColorBase: Clothing
	{
		itemSize[]={3,1};
	};
	class Wellies_ColorBase: Clothing
	{
		attachments[]=
		{
			"Knife"
		};
		itemSize[]={3,4};
		weight=1700;
		heatIsolation=0.5;
		radiationProtection = 0.95;
		// varQuantityInit=0;
        // varQuantityMin=0;
        // varQuantityMax=5000;
        // destroyOnEmpty=0;
        // varQuantityDestroyOnMin=0;
        // varLiquidTypeInit=512;
        // varTemperatureMax=200;
        // liquidContainerType="1 + 2 + 4 + 8 + 16 + 32 + 64 + 128 + 256 + 512 + 1024 + 2048 + 4096 + 8192 + 16384 + 32768 + 65536  - (1 + 2 + 4 + 8 + 16 + 32 + 64 + 128 + 256) -32768";
        
		// class Protection
		// {
		//     nuclear = 0.9;
		// };
	};
	class WorkingBoots_ColorBase: Clothing
	{
		itemSize[]={3,2};
	};
	class JungleBoots_ColorBase: Clothing
	{
		attachments[]=
		{
			"Knife"
		};
		itemSize[]={3,3};
	};
	class DressShoes_ColorBase: Clothing
	{
		itemSize[]={3,1};
	};
	class MilitaryBoots_ColorBase: Clothing
	{
		itemSize[]={3,3};
	};
	class CombatBoots_ColorBase: Clothing
	{
		itemSize[]={3,2};
	};
	class JoggingShoes_ColorBase: Clothing
	{
		itemSize[]={3,1};
	};
	class LeatherShoes_ColorBase: Clothing
	{
		itemSize[]={3,1};
	};
	class Sneakers_ColorBase: Clothing
	{
		itemSize[]={3,1};
	};
	class Ballerinas_ColorBase: Clothing
	{
		itemSize[]={3,1};
	};
	class FeetCover_Improvised: Clothing
	{
		itemSize[]={2,1};
	};
	class TTSKOBoots: Clothing
	{
		itemSize[]={3,3};
	};

	class MedievalBoots: Clothing
	{
		attachments[]=
		{
			"Knife"
		};
		itemSize[]={3,4};
		varWetMax=0.49000001;
	};
	
	class NBCBootsBase: Clothing
	{
		attachments[]=
		{
			"Feet"
		};
		itemSize[]={2,1};
		radiationProtection = 0.99;
	};
	


// VESTS
	class SmershVest: Clothing
	{
		itemsCargoSize[]={6,2};
	};
	class PressVest_ColorBase: Clothing
	{
		itemsCargoSize[]={6,2};
	};
	class UKAssVest_ColorBase: Clothing
	{
		itemsCargoSize[]={7,2};
	};
	class HighCapacityVest_ColorBase: Clothing
	{
		itemsCargoSize[]={8,2};
	};
	class LeatherStorageVest_ColorBase: Clothing
	{
		itemsCargoSize[]={5,2};
	};
	class HuntingVest: Clothing
	{
		itemsCargoSize[]={5,2};
	};


// BELTS
	class HipPack_ColorBase: Clothing
	{
		itemsCargoSize[]={3,1};
	};

// BAGS
	class TaloonBag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,6};
	};
	class TortillaBag: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={6,7};
	};
	class CourierBag: Clothing
	{
		itemsCargoSize[]={5,5};
	};
	class FurCourierBag: Clothing
	{
		itemsCargoSize[]={5,5};
	};
	class ImprovisedBag: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,5};
	};
	class FurImprovisedBag: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,5};
	};
	class DryBag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={6,7};
	};
	class HuntingBag: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={6,6};
	};
	class MountainBag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={7,8};
	};
	class SmershBag: Clothing
	{
		itemsCargoSize[]={5,4};
	};
	class ChildBag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,5};
	};
	class LeatherSack_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={6,7};
	};
	class AssaultBag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,6};
	};
	class CoyoteBag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={6,7};
	};
	class AliceBag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={8,8};
	};
	class SlingBag_ColorBase: Clothing
	{
		itemsCargoSize[]={4,5};
	};
	class ArmyPouch_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,5};
	};
	class DuffelBagSmall_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,4};
	};
	class CanvasBag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,4};
	};

	class BurlapSack: Inventory_Base
	{
		itemSize[]={5,5};
		itemsCargoSize[]={5,5};
	};


//Ghillie
	class GhillieHood_ColorBase: Clothing
	{
		inventorySlot[]=
		{
			"cape"
		};
	};	
		
	class GhillieBushrag_ColorBase : Clothing
	{
		inventorySlot[]=
		{
			"Armband"
		};
		itemInfo[]=
		{
			"Clothing",
			"Armband"
		};
	};
  		
	class GhillieTop_ColorBase : Clothing
	{
		inventorySlot[]=
		{
			"Armband"
		};
		itemInfo[]=
		{
			"Clothing",
			"Armband"
		};
	};
	
	class GhillieSuit_ColorBase : Clothing
	{
		inventorySlot[]=
		{
			"Armband"
		};
		itemInfo[]=
		{
			"Clothing",
			"Armband"
		};
	};


// DRINKS
	class SodaCan_ColorBase: Edible_Base
	{
		varQuantityInit=333;
		varQuantityMax=333;
		class Nutrition
		{
			totalVolume=1;
			energy=42;
			water=100;
			nutritionalIndex=1;
			toxicity=0;
		};
	};

	class SodaCan_SodaCan_Kvass: SodaCan_ColorBase
	{
		medRadiationIncrement = -2;
	};

	class Bottle_Base;
	class Vodka: Bottle_Base
	{
		class Nutrition
		{
			totalVolume=1;
			energy=230;
			water=60;
			nutritionalIndex=1;
			toxicity=0;
			agents=4;
		};
		medRadiationIncrement = -16;
	}

// FOOD
	class BoxCerealCrunchin: Edible_Base
	{
		varQuantityInit=255;
		varQuantityMax=255;
		class Nutrition
		{
			fullnessIndex=2;
			energy=419;
			water=0;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class PowderedMilk: Edible_Base
	{
		class Nutrition
		{
			fullnessIndex=2;
			energy=496;
			water=0;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class SmallGuts: Edible_Base
	{
		medRadiationIncrement = 2;
		class Nutrition
		{
			fullnessIndex=10;
			energy=200;
			water=75/4;
			nutritionalIndex=1;
			toxicity=0;
			agents=4;
		};
	};
	class Guts: Edible_Base
	{
		medRadiationIncrement = 2;
		class Nutrition
		{
			fullnessIndex=10;
			energy=200;
			water=75/4;
			nutritionalIndex=1;
			toxicity=0;
			agents=4;
		};
	};
	class Rice: Edible_Base
	{
		varQuantityInit=1000;
		varQuantityMax=1000;
		class Nutrition
		{
			fullnessIndex=10;
			energy=130;
			water=0;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class Marmalade: Edible_Base
	{
		varQuantityInit=340;
		varQuantityMax=340;
		class Nutrition
		{
			fullnessIndex=4;
			energy=246;
			water=25/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class Honey: Edible_Base
	{
		class Nutrition
		{
			fullnessIndex=5;
			energy=1216;
			water=20/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};

	class Zagorky_ColorBase: Edible_Base
	{
		varQuantityInit=33;
		varQuantityMax=33;
	};
	class Zagorky: Zagorky_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=2;
			energy=462;
			water=0;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class ZagorkyChocolate: Zagorky_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=2;
			energy=555;
			water=0;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class ZagorkyPeanuts: Zagorky_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=2;
			energy=507;
			water=0;
			nutritionalIndex=1;
			toxicity=0;
		};
	};

	class Snack_ColorBase: Edible_Base
	{

	};
	class SaltySticks: Snack_ColorBase
	{
		varQuantityInit=30;
		varQuantityMax=30;
		class Nutrition
		{
			fullnessIndex=3;
			energy=374;
			water=0;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class Crackers: Snack_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=3;
			energy=400;
			water=0;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class Chips: Snack_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=3;
			energy=536;
			water=0;
			nutritionalIndex=1;
			toxicity=0;
		};
	};

	class BakedBeansCan_Opened: Edible_Base
	{
		varQuantityInit=415;
		varQuantityMax=415;
		class Nutrition
		{
			fullnessIndex=2;
			energy=75;
			water=73/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class PeachesCan_Opened: Edible_Base
	{
		varQuantityInit=425;
		varQuantityMax=425;
		class Nutrition
		{
			fullnessIndex=2;
			energy=78;
			water=90/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class TacticalBaconCan_Opened: Edible_Base
	{
		varQuantityInit=255;
		varQuantityMax=255;
		class Nutrition
		{
			fullnessIndex=2;
			energy=424;
			water=10/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class SpaghettiCan_Opened: Edible_Base
	{
		varQuantityInit=425;
		varQuantityMax=425;
		class Nutrition
		{
			fullnessIndex=2;
			energy=92;
			water=75/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class SardinesCan_Opened: Edible_Base
	{
		varQuantityInit=125;
		varQuantityMax=125;
		class Nutrition
		{
			fullnessIndex=2;
			energy=208;
			water=62/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class TunaCan_Opened: Edible_Base
	{
		class Nutrition
		{
			fullnessIndex=2;
			energy=128;
			water=73/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class FoodCan_250g_Opened_ColorBase: Edible_Base
	{
	};
	class DogFoodCan_Opened: FoodCan_250g_Opened_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=10;
			energy=106;
			water=75/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class CatFoodCan_Opened: FoodCan_250g_Opened_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=10;
			energy=121;
			water=75/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class PorkCan_Opened: FoodCan_250g_Opened_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=2;
			energy=225;
			water=75/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class Lunchmeat_Opened: FoodCan_250g_Opened_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=2;
			energy=125;
			water=20/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};

	class CrabCan_Opened : FoodCan_250g_Opened_ColorBase
	{
		class Nutrition
		{
			fullnessIndex = 10;
			energy = 83;
			water = 80/4;
			nutritionalIndex = 1;
			toxicity = 0;
		};
	};

	class UnknownFoodCan_Opened: Edible_Base
	{
		class Nutrition
		{
			fullnessIndex=10;
			energy=125;
			water=75/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class FoodCan_100g_Opened_ColorBase: Edible_Base
	{
	};
	class Pajka_Opened: FoodCan_100g_Opened_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=2;
			energy=306;
			water=75/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class Pate_Opened: FoodCan_100g_Opened_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=2;
			energy=319;
			water=75/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};
	class BrisketSpread_Opened: FoodCan_100g_Opened_ColorBase
	{
		class Nutrition
		{
			fullnessIndex=2;
			energy=253;
			water=50/4;
			nutritionalIndex=1;
			toxicity=0;
		};
	};

	class Apple: Edible_Base
	{
		medRadiationIncrement = 1;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={1,39,85/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,19,85/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={1,52,42/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={1,52,85/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={1,52,21/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,19,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};
	class Plum: Edible_Base
	{
		medRadiationIncrement = 1;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={1,35,88/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,17,88/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={1,46,44/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={1,46,88/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={1,46,22/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,17,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};
	class Pear: Edible_Base
	{
		medRadiationIncrement = 1;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={1,43,84/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,22,84/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={1,57,42/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={1,57,84/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={1,57,21/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,22,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};
	class Banana: Edible_Base
	{
		medRadiationIncrement = 1;
	};
	class Orange: Edible_Base
	{
		medRadiationIncrement = 1;
	};
	class Tomato: Edible_Base
	{
		medRadiationIncrement = 1;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={1,14,95/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,7,95/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={1,18,48/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={1,18,95/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={1,18,24/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,7,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};
	class GreenBellPepper: Edible_Base
	{
		medRadiationIncrement = 1;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={1,15,94/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,9,94/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={1,20,47/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={1,20,94/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={1,20,23/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,9,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};
	class Zucchini: Edible_Base
	{
		medRadiationIncrement = 1;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={1,22,96/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,11,96/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={1,27,47/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={1,27,96/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={1,27,23/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,11,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};
	class SlicedPumpkin: Edible_Base
	{
		medRadiationIncrement = 1;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={1,20,92/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,10,92/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={1,26,46/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={1,26,92/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={1,26,23/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,10,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};
	class Potato: Edible_Base
	{
		medRadiationIncrement = 1;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={1,65,78/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,33,78/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={1,87,39/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={1,87,78/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={1,87,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,33,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};
	class Kiwi: Edible_Base
	{
		medRadiationIncrement = 1;
	};
	class SambucusBerry: Edible_Base
	{
		medRadiationIncrement = 2;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={1,55,80/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,28,80/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={1,73,40/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={1,73,80/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={1,73,20/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,28,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};
	class CaninaBerry: Edible_Base
	{
		medRadiationIncrement = 2;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={1,122,59/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,61,59/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={1,162,29/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={1,162,59/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={1,162,15/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,61,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};
	class Cannabis: Edible_Base
	{
		medRadiationIncrement = 2;
		medMindDegradationForce = 1;
    	medMindDegradationTime = 5;
	};

	class HumanSteakMeat: Edible_Base
	{
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,98,77/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,49,77/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,130,39/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,130,77/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,130,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,49,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class GoatSteakMeat: Edible_Base
	{
		medRadiationIncrement = 1;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,106,77/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,53,77/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,142,39/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,142,77/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,142,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,53,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class MouflonSteakMeat: Edible_Base
	{
		medRadiationIncrement = 2;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,137,75/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,69,75/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,183,37/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,183,75/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,183,18/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,53,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class BoarSteakMeat: Edible_Base
	{
		medRadiationIncrement = 2;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,110,75/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,55,75/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,147,38/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,147,75/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,147,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,55,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class PigSteakMeat: Edible_Base
	{
		medRadiationIncrement = 1;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,194,68/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,97,68/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,259,34/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,259,68/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,259,17/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,97,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class DeerSteakMeat: Edible_Base
	{
		medRadiationIncrement = 2;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,143,74/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,71,74/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,190,37/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,190,74/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,190,18/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,71,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class WolfSteakMeat: Edible_Base
	{
		medRadiationIncrement = 3;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,197,70/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,98,70/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,262,35/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,262,70/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,262,17/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,98,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class BearSteakMeat: Edible_Base
	{
		medRadiationIncrement = 3;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,194,71/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,97,71/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,258,35/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,258,71/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,258,17/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,97,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class CowSteakMeat: Edible_Base
	{
		medRadiationIncrement = 1;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,188,76/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,94,76/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,250,38/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,250,76/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,250,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,94,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class SheepSteakMeat: Edible_Base
	{
		medRadiationIncrement = 1;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,137,75/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,69,75/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,183,37/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,183,75/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,183,18/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,53,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class FoxSteakMeat: Edible_Base
	{
		medRadiationIncrement = 2;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,106,77/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,53,77/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,142,39/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,142,77/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,142,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,53,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class ChickenBreastMeat: Edible_Base
	{
		medRadiationIncrement = 1;
		weightPerQuantityUnit=1;
		varQuantityInit=250;
		varQuantityMax=250;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,124,75/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,62,75/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,165,38/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,165,75/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,165,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,62,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class RabbitLegMeat: Edible_Base
	{
		medRadiationIncrement = 2;
		weightPerQuantityUnit=1;
		varQuantityInit=250;
		varQuantityMax=250;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,130,76/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,53,76/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,173,38/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,173,76/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,173,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,53,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class CarpFilletMeat: Edible_Base
	{
		medRadiationIncrement = 2;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,122,76/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,61,76/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,162,38/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,162,76/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,162,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,61,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class MackerelFilletMeat: Edible_Base
	{
		medRadiationIncrement = 2;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,101,76/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,50,76/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,134,38/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,134,77/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,134,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,50,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class Worm: Edible_Base
	{
		medRadiationIncrement = 2;
	};

	class Lard: Edible_Base
	{
		medRadiationIncrement = 1;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,351,48/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,175,48/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,468,24/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,468,48/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,468,12/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,175,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class Bitterlings: Edible_Base
	{
		medRadiationIncrement = 2;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,122,76/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,61,76/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,162,38/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,162,76/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,162,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,61,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class Sardines: Edible_Base
	{
		medRadiationIncrement = 2;
		weightPerQuantityUnit=1;
		varQuantityInit=500;
		varQuantityMax=500;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={5,156,76/4,1,0,4};
				};
				class Rotten
				{
					nutrition_properties[]={10,78,76/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,208,38/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,208,76/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={3,208,19/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,78,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class RedCaviar : Edible_Base
	{
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[] = { 3,260,48/4,1,0 };
				};
				class Rotten
				{
					nutrition_properties[] = {10,130,48/4,1,0,16 };
				};
			};
		};
	};

	class MushroomBase: Edible_Base
	{
		medRadiationIncrement = 2;
	};

	class AgaricusMushroom: MushroomBase
	{
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={3,17,92/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,8,92/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,22,46/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,22,92/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={2,22,22/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,8,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class AmanitaMushroom: MushroomBase
	{
		medMindDegradationForce = 1;
    	medMindDegradationTime = 20;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={3,17,92/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,8,92/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,22,46/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,22,92/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={2,22,23/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,8,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class MacrolepiotaMushroom: MushroomBase
	{
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={3,35,84/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,17,84/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,47,42/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,47,84/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={2,47,21/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,17,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class LactariusMushroom: MushroomBase
	{
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={3,22,92/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,11,92/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,29,46/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,29,92/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={2,29,23/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,11,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class PsilocybeMushroom: MushroomBase
	{
		medMindDegradationForce = 1;
    	medMindDegradationTime = 20;
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={3,17,92/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,8,92/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,22,46/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,22,92/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={2,22,23/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,8,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class AuriculariaMushroom: MushroomBase
	{
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={3,19,90/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,9,90/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,25,45/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,25,90/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={2,25,22/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,9,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class BoletusMushroom: MushroomBase
	{
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={3,36,85/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,18,85/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,48,42/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,48,85/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={2,48,21/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,18,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class PleurotusMushroom: MushroomBase
	{
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[]={3,25,90/4,1,0};
				};
				class Rotten
				{
					nutrition_properties[]={10,12,90/4,1,0,16};
				};
				class Baked
				{
					nutrition_properties[]={2,33,45/4,1,0};
					cooking_properties[]={70,240};
				};
				class Boiled
				{
					nutrition_properties[]={2,33,90/4,1,0};
					cooking_properties[]={70,360};
				};
				class Dried
				{
					nutrition_properties[]={2,33,22/4,1,0};
					cooking_properties[]={70,90};
				};
				class Burned
				{
					nutrition_properties[]={10,12,0,1,0,16};
					cooking_properties[]={100,120};
				};
			};
		};
	};

	class CraterellusMushroom : MushroomBase
	{
		class Food
		{
			class FoodStages
			{
				class Raw
				{
					nutrition_properties[] = { 3,38,90/4,1,0 };
				};
				class Rotten
				{
					nutrition_properties[] = { 10,19,90/4,1,0,16 };
				};
				class Baked
				{
					nutrition_properties[] = { 2,57,45/4,1,0 };
					cooking_properties[] = { 70,240 };
				};
				class Boiled
				{
					nutrition_properties[] = { 2,57,90/4,1,0 };
					cooking_properties[] = { 70,360 };
				};
				class Dried
				{
					nutrition_properties[] = { 2,57,22/4,1,0 };
					cooking_properties[] = { 70,90 };
				};
				class Burned
				{
					nutrition_properties[] = { 10,12,0,1,0,16 };
					cooking_properties[] = { 100,120 };
				};
			};
		};
	};


	class BandageDressing: Inventory_Base
	{
		weightPerQuantityUnit=30.0/2.0;
		varQuantityInit=4*2;
		varQuantityMax=4*2;
	};

	class DisinfectantSpray: Edible_Base
	{
		weightPerQuantityUnit=1.0/4.0;
		varQuantityInit=500*4;
		varQuantityMax=500*4;
	};

	class DisinfectantAlcohol: Edible_Base
	{
		weightPerQuantityUnit=1.0/2.0;
		varQuantityInit=200*2;
		varQuantityMax=200*2;
	};

	class IodineTincture: Edible_Base
	{
		weightPerQuantityUnit=1.0/2.0;
		varQuantityInit=250*2;
		varQuantityMax=2500*2;
	};


	class DuctTape: Inventory_Base
	{
		weightPerQuantityUnit=5.0/8.0;
		varQuantityInit=100*8;
		varQuantityMax=100*8;
	};

	class Battery9V: Inventory_Base
	{
		class EnergyManager
		{
			energyStorageMax=500;
			energyAtSpawn=500;
		};
	};

	class LargeGasCanister: Inventory_Base
	{
		weightPerQuantityUnit=0.51999998/10.0;
		class EnergyManager
		{
			energyStorageMax=8000;
			energyAtSpawn=8000;
		};
	};

	class MediumGasCanister: Inventory_Base
	{
		weightPerQuantityUnit=0.31999999/10.0;
		class EnergyManager
		{
			energyStorageMax=6500;
			energyAtSpawn=6500;
		};
	};

	class SmallGasCanister: Inventory_Base
	{
		weightPerQuantityUnit=0.28/10.0;
		class EnergyManager
		{
			energyStorageMax=3300;
			energyAtSpawn=3300;
		};
	};

	class GardenLime: Inventory_Base
	{
		class Horticulture
		{
			ConsumedQuantity=50;
		};
	};


	class SewingKit: Inventory_Base
	{
		weightPerQuantityUnit=0.2/8.0;
		varQuantityInit=100*8;
		varQuantityMax=100*8;
	};

	class LeatherSewingKit: Inventory_Base
	{
		weightPerQuantityUnit=0.30000001/8.0;
		varQuantityInit=100*8;
		varQuantityMax=100*8;
	};

	class ElectronicRepairKit: Inventory_Base
	{
		varQuantityInit=100*8;
		varQuantityMax=100*8;
	};

	class EpoxyPutty: Inventory_Base
	{
		weightPerQuantityUnit=2.0/8.0;
		varQuantityInit=100*8;
		varQuantityMax=100*8;
	};

	class Whetstone: Inventory_Base
	{
		varQuantityInit=100*8;
		varQuantityMax=100*8;
	};

	class WeaponCleaningKit: Inventory_Base
	{
		varQuantityInit=100*8;
		varQuantityMax=100*8;
	};


	class PetrolLighter: Inventory_Base
	{
		varQuantityInit=20*8;
		varQuantityMax=20*8;
	};

	class TireRepairKit: Inventory_Base
	{
		varQuantityInit=100*8;
		varQuantityMax=100*8;
	};
	

	class TruckBattery: Inventory_Base
	{
		class EnergyManager
		{
			energyStorageMax=12000;
			energyAtSpawn=12000;
		};
	};

	class CarBattery: Inventory_Base
	{
		class EnergyManager
		{
			energyStorageMax=4000;
			energyAtSpawn=4000;
		};
	};



	class WoodenPlank: Inventory_Base
	{
		itemSize[]={3,9};
	};

	class BarbedWireLocked: Inventory_Base
	{
		StruggleLength=240;
	};

	class DuctTapeLocked: Inventory_Base
	{
		StruggleLength=60;
	};

	class RopeLocked: Inventory_Base
	{
		StruggleLength=120;
	};

	class MetalWireLocked: Inventory_Base
	{
		StruggleLength=180;
	};

	class HandcuffsLocked: Inventory_Base
	{
		StruggleLength=300;
	};


	// class DZ_LightAI;
	// class AnimalBase: DZ_LightAI
	// {
	// };

	//Syberia

	// class Otter_Base : AnimalBase
	// {
	//     knifeDamageModifier = 0.25;
	//     class Cargo
	//     {
	//         itemsCargoSize[] = { 10,4 };
	//         allowOwnedCargoManipulation = 1;
	//         openable = 0;
	//     };
	// };

	// class Animal_Otter : Otter_Base
	// {
	//     knifeDamageModifier = 0.25;
	//     class Cargo
	//     {
	//         itemsCargoSize[] = { 10,4 };
	//         allowOwnedCargoManipulation = 1;
	//         openable = 0;
	//     };
	// };

	// class Rabbit_Base : AnimalBase
	// {
	//     knifeDamageModifier = 0.25;
	//     class Cargo
	//     {
	//         itemsCargoSize[] = { 10,4 };
	//         allowOwnedCargoManipulation = 1;
	//         openable = 0;
	//     };
	// };

	// // class Rat_Base : AnimalBase
	// // {
	// //     knifeDamageModifier = 0.25;
	// //     class Cargo
	// //     {
	// //         itemsCargoSize[] = { 10,4 };
	// //         allowOwnedCargoManipulation = 1;
	// //         openable = 0;
	// //     };
	// // };

	 class DeadRat_ColorBase : Edible_Base{};
     class DeadSquirrel_Base : Edible_Base{};

//DayZ animals
	//class SkinnedRat: AnimalBase
	//{
	//	medRadiationIncrement = 2;
	//};

	//class OtterSteakMeat : Edible_Base
 //   {	
 //   	medRadiationIncrement = 2;
	//	weightPerQuantityUnit = 1;
	//	varQuantityInit = 500;
	//	varQuantityMax = 500;
	//	class Food
	//	{
	//		class FoodStages
	//		{
	//			class Raw
	//			{
	//				nutrition_properties[]={5,159,75,1,0,4};
	//			};
	//			class Rotten
	//			{
	//				nutrition_properties[]={10,80,75,1,0,16};
	//			};
	//			class Baked
	//			{
	//				nutrition_properties[]={2,212,37,1,0};
	//				cooking_properties[]={70,240};
	//			};
	//			class Boiled
	//			{
	//				nutrition_properties[]={2,212,75,1,0};
	//				cooking_properties[]={70,360};
	//			};
	//			class Dried
	//			{
	//				nutrition_properties[]={3,212,18,1,0};
	//				cooking_properties[]={70,90};
	//			};
	//			class Burned
	//			{
	//				nutrition_properties[]={10,61,0,1,0,16};
	//				cooking_properties[]={100,120};
	//			};
	//		};
	//	};
	//};

//BL
   class bl_improvised_tape : DuctTape
   {
       itemSize[]={1, 2};
   };

   class bl_candle : Inventory_Base
   {
   		class EnergyManager
        {
            energyAtSpawn=7200;
        };
   };

////Zeroy
//	class CodFilletMeat: Edible_Base
//	{
//		medRadiationIncrement = 2;
//		weightPerQuantityUnit=1;
//		varQuantityInit=500;
//		varQuantityMax=500;
//		class Food
//		{
//			class FoodStages
//			{
//				class Raw
//				{
//					nutrition_properties[]={5,79,81/4,1,0,4};
//				};
//				class Rotten
//				{
//					nutrition_properties[]={10,39,81/4,1,0,16};
//				};
//				class Baked
//				{
//					nutrition_properties[]={2,105,40/4,1,0};
//					cooking_properties[]={70,240};
//				};
//				class Boiled
//				{
//					nutrition_properties[]={2,105,81/4,1,0};
//					cooking_properties[]={70,360};
//				};
//				class Dried
//				{
//					nutrition_properties[]={3,105,20/4,1,0};
//					cooking_properties[]={70,90};
//				};
//				class Burned
//				{
//					nutrition_properties[]={10,39,0,1,0,16};
//					cooking_properties[]={100,120};
//				};
//			};
//		};
//	};
//	class PikeFilletMeat: Edible_Base
//	{
//		medRadiationIncrement = 2;
//		weightPerQuantityUnit=1;
//		varQuantityInit=500;
//		varQuantityMax=500;
//		class Food
//		{
//			class FoodStages
//			{
//				class Raw
//				{
//					nutrition_properties[]={5,85,79/4,1,0,4};
//				};
//				class Rotten
//				{
//					nutrition_properties[]={10,42,79/4,1,0,16};
//				};
//				class Baked
//				{
//					nutrition_properties[]={2,113,39/4,1,0};
//					cooking_properties[]={70,240};
//				};
//				class Boiled
//				{
//					nutrition_properties[]={2,113,79/4,1,0};
//					cooking_properties[]={70,360};
//				};
//				class Dried
//				{
//					nutrition_properties[]={3,113,19/4,1,0};
//					cooking_properties[]={70,90};
//				};
//				class Burned
//				{
//					nutrition_properties[]={10,42,0,1,0,16};
//					cooking_properties[]={100,120};
//				};
//			};
//		};
//	};
//	class SalmonFilletMeat: Edible_Base
//	{
//		medRadiationIncrement = 2;
//		weightPerQuantityUnit=1;
//		varQuantityInit=500;
//		varQuantityMax=500;
//		class Food
//		{
//			class FoodStages
//			{
//				class Raw
//				{
//					nutrition_properties[]={5,136,72/4,1,0,4};
//				};
//				class Rotten
//				{
//					nutrition_properties[]={10,68,72/4,1,0,16};
//				};
//				class Baked
//				{
//					nutrition_properties[]={2,182,36/4,1,0};
//					cooking_properties[]={70,240};
//				};
//				class Boiled
//				{
//					nutrition_properties[]={2,182,72/4,1,0};
//					cooking_properties[]={70,360};
//				};
//				class Dried
//				{
//					nutrition_properties[]={3,182,18/4,1,0};
//					cooking_properties[]={70,90};
//				};
//				class Burned
//				{
//					nutrition_properties[]={10,68,0,1,0,16};
//					cooking_properties[]={100,120};
//				};
//			};
//		};
//	};
//	class zeroy_chub: Edible_Base
//	{
//		medRadiationIncrement = 2;
//		weightPerQuantityUnit=1;
//		varQuantityInit=500;
//		varQuantityMax=500;
//		class Food
//		{
//			class FoodStages
//			{
//				class Raw
//				{
//					nutrition_properties[]={5,147,76/4,1,0,4};
//				};
//				class Rotten
//				{
//					nutrition_properties[]={10,73,76/4,1,0,16};
//				};
//				class Baked
//				{
//					nutrition_properties[]={2,196,38/4,1,0};
//					cooking_properties[]={70,240};
//				};
//				class Boiled
//				{
//					nutrition_properties[]={2,196,76/4,1,0};
//					cooking_properties[]={70,360};
//				};
//				class Dried
//				{
//					nutrition_properties[]={3,196,19/4,1,0};
//					cooking_properties[]={70,90};
//				};
//				class Burned
//				{
//					nutrition_properties[]={10,73,0,1,0,16};
//					cooking_properties[]={100,120};
//				};
//			};
//		};
//	};
//	class zeroy_shell: Edible_Base
//	{
//		medRadiationIncrement = 2;
//		weightPerQuantityUnit=1;
//		varQuantityInit=50;
//		varQuantityMax=50;
//		class Food
//		{
//			class FoodStages
//			{
//				class Raw
//				{
//					nutrition_properties[]={5,103,76/4,1,0,4};
//				};
//				class Rotten
//				{
//					nutrition_properties[]={10,51,76/4,1,0,16};
//				};
//				class Baked
//				{
//					nutrition_properties[]={2,137,38/4,1,0};
//					cooking_properties[]={70,240};
//				};
//				class Boiled
//				{
//					nutrition_properties[]={2,137,76/4,1,0};
//					cooking_properties[]={70,360};
//				};
//				class Dried
//				{
//					nutrition_properties[]={3,137,19/4,1,0};
//					cooking_properties[]={70,90};
//				};
//				class Burned
//				{
//					nutrition_properties[]={10,51,0,1,0,16};
//					cooking_properties[]={100,120};
//				};
//			};
//		};
//	};
//
//	class Zeroy_fishing_Pants_Camo: NBCPantsBase
//	{
//		radiationProtection = 0.95;
//		itemsCargoSize[]={2,1};
//		// class Protection
//		// {
//		//     nuclear = 0.9;
//		// };
//	};
//
//	class zeroy_fishing_backpack_ColorBase: Clothing
//	{
//		attachments[]+=
//		{

//			"Compass",
//			"Paper",
//			"Pen"
//		};
//		itemsCargoSize[]={8,7};
//	};
//
//	class zeroy_tacklebox_ColorBase: Container_Base
//	{
//		itemSize[]={6,4};
//		itemsCargoSize[]={6,4};
//		varWetMax=0.249;
//	};

//Windstride
	class PMK_5A_Gas_Mask: Clothing
	{
		itemsCargoSize[]={0,0};
	};

	class Leather_Cloak_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};
	};

	class Defender_Vest_ColorBase : Clothing
    {
    	bulletProofProtection = 0.4;
    };

	class Medium_Sleeves_Shirt: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class Military_Sweater: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class Layered_Shirt_Base: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class Shabby_Hoodie_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class Winter_Parka_Base: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Winter_Parka_Green: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Wool_GreatCoat_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};

	class Galife_Pants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={5,1};
		notOutside=1;
	};
	class Kneepads_Jeans_Base: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={7,1};
		notOutside=1;
	};
	class Legging_Shorts_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};
		notOutside=1;
	};
	class Leggings_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};
		notOutside=1;
	};
	class Skinny_Jeans_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
		notOutside=1;
	};

	class FlipFlops_Colorbase : Clothing
    {
    	itemSize[]={3,2};
    };

    class High_Knee_Sneakers : Clothing
    {
    	attachments[]=
		{
			"Knife"
		};
    	itemSize[]={3,4};
    };

	class Kirza_Boots_ColorBase: Clothing
	{
		attachments[]=
		{
			"Knife"
		};
		itemSize[]={3,4};
	};

	class Canvas_Backpack_Base : TaloonBag_ColorBase
	{
		attachments[]=
		{
			"Pin1", 
			"Pin2", 
			"Pin3", 
			"Chemlight", 
			"WalkieTalkie", 
			"WaterBottle", 
			"DummySlot", 
			"Machete", 
			"KukriKnife", 
			"FangeKnife", 
			"CrudeMachete", 
			"OrientalMachete", 
			"Hammer", 
			"Hatchet", 
			"SawedoffMosin9130", 
			"SawedoffB95", 
			"SawedoffIzh18", 
			"SawedoffIzh18Shotgun", 
			"SawedoffIzh43Shotgun", 
			"MassMP153Short", 
			"MassStevens301SuperShort",

			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,6};
	};


////Munghard
	class M52helmet_mung : Clothing
   {
   	meleeProtection = 0.9;
   	firearmProtection = 0.3;
   };

	class pilotjacket_mung: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};

	class milipants_mung: Clothing
	{
		itemsCargoSize[]={8,1};
	};
		class stalkerpants_mung: Clothing
	{
		itemsCargoSize[]={8,1};
	};

	class dappervest_mung : Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class bikervest_mung: Clothing
	{
		itemsCargoSize[]={1,2};
	};
	class tacticalvest_mung: Clothing
	{
		itemsCargoSize[]={8,2};
	};
	class tacvest_mung: Clothing
	{
		itemsCargoSize[]={1,2};
	};

	class fannypack_ColorBase_mung : Clothing
   {
       itemsCargoSize[]={4,1};
   };

	class simplebackpack_ColorBase_mung: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
	};
	class jaakari_Colorbase_mung: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={7,8};
	};
	// class jaakari_simple_Colorbase_mung: jaakari_Colorbase_mung
	// {
	// 	attachments[]+=
	// 	{
	// 		"Compass",
	// 		"Paper",
	// 		"Pen"
	// 	};
	// };
	class bag_6B38_Colorbase_mung: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={8,8};
	};
	class Hikingbagmung_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={8,8};
	};

	class carrierrig_mung: SmershBag
	{
		varWetMax=0.249;
	};

	class stalker_gasmask_colorbase_mung: Clothing
	{
	    radiationProtection = 0.99;
	    // class Protection
		// {
		//     nuclear = 1.0;
		// };
	};
	class mag3_gasmask_mung: Clothing
	{
	    radiationProtection = 0.99;
	    // class Protection
		// {
		//     nuclear = 1;
		// };
	};

	class sandwich_mung : Edible_Base
   {
       class Nutrition
       {
           fullnessIndex=2;
           energy=300;
           water=75/4;
           nutritionalIndex=1;
           toxicity=0;
       };
   };

   class wonderbread_mung : Edible_Base
   {
       varQuantityInit=500;
       varQuantityMax=500;
       class Nutrition
       {
           fullnessIndex=5;
           energy=313;
           water=35/4;
           nutritionalIndex=1;
           toxicity=0;
       };
   };


//Loft
	class Loftd_bagbelt_ColorBase : Clothing
    {
		itemsCargoSize[]={4,1};
	};
	class Loftd_toolbelt_ColorBase : Clothing
    {
		itemsCargoSize[]={4,1};
	};

	class Loftd_civbag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,6};
	};
	class Loftd_bugbag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,5};
	};
	class Loftd_bunnybag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,5};
	};
	class Loftd_elliebag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,6};
	};
	class Loftd_hollybag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={6,6};
	};
	class Loftd_leatherBag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={4,4};
	};
	class Loftd_reinabag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={4,7};
	};
	class Loftd_slackerbag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"Paper",
			"Wallet"
		};
		itemsCargoSize[]={6,6};
	};
	class Loftd_survivorbag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,6};
	};
	class Loftd_teddybag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,5};
	};
	class Loftd_Town_bag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,5};
	};
	class Loftd_Townbag_pouch_ColorBase: Container_Base
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_Townbag_pouch2_ColorBase: Container_Base
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_travelsbag_ColorBase: Clothing
	{
		itemsCargoSize[]={8,5};
	};
	class Loftd_valeriebag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,5};
	};
	class Loftd_womensbag_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class Loftd_shadowbag_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,6};
	};
	class Loftd_Assaultbackpack_ColorBase: Clothing
	{
		attachments[]+=
		{
			"mapbag", 
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={5,7};
	};


	class Loftd_Arcpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_bakedpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_cargopants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_cargoshorts_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
		notOutside=1;
	};
	class Loftd_cyberpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={4,1};
	};
	class Loftd_jeans: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
	};
	class Loftd_ririteopants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_tifan_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
	};
	class Loftd_westpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={10,1};
	};

	class Loftd_acronym_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={10,1};
	};
	class Loftd_adidasjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class Loftd_aidansuit_jacket_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class Loftd_aidansuit_pants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_albert_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class Loftd_alexjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_armyjacket: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_bajka_ColorBase: Clothing
	{
		itemsCargoSize[]={4,1};
	};
	class Loftd_bikerjacket: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_bomberjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_brian_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_Brianpug_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_bridgejacket: Clothing
	{
		itemsCargoSize[]={4,1};
	};
	class Loftd_brundonjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={6,1};
	};
	class Loftd_cybershirt_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class Loftd_femalejacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,1};
	};
	class Loftd_gaptopsuit_jacket_ColorBase: Clothing
	{
		itemsCargoSize[]={6,1};
	};
	class Loftd_gaptopsuit_jacket_CL_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class Loftd_gaptopsuit_pants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
		notOutside=1;
	};
	class Loftd_hoodedjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class Loftd_jacketfranc_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class Loftd_jackethen_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_jacketlong_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_leatherjacket: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_leatherjacketW: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
		varWetMax=0.249;
	};
	class Loftd_northjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={6,1};
	};
	class Loftd_Reinajacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,1};
	};
	class Loftd_Reinapants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,1};
		notOutside=1;
	};
	class Loftd_Ririteojacket_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class Loftd_roadiessuit_jacket_ColorBase: Clothing
	{
		itemsCargoSize[]={4,1};
	};
	class Loftd_roadiessuit_pants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
	};
	class Loftd_shirt_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class Loftd_shirtJ_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class Loftd_suedejacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_waistcoat_ColorBase: Clothing
	{
		itemsCargoSize[]={5,1};
	};
	class Loftd_windjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={10,1};
	};
	class Loftd_wolverine_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_wooljacket: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_wornjacket_ColorBase
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};

	class Loftd_bloodvest_ColorBase: Clothing
	{
		itemsCargoSize[]={10,2};
	};
	class Loftd_civilvest_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_huntervest_ColorBase: Clothing
	{
		itemsCargoSize[]={7,2};
	};

	class Loftd_blackpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
		notOutside=1;
	};
	class Loftd_carharttpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_jeanspants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
		notOutside=1;
	};
	class Loftd_kneepadspants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={4,1};
		notOutside=1;
	};
	class Loftd_Leggings_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};
		notOutside=1;
	};
	class Loftd_urbanpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={10,1};
	};

	class Loftd_akito_jacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={9,1};
		varWetMax=0.49000001;
	};
	class Loftd_akito_pants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={9,1};
		varWetMax=0.49000001;
		notOutside=1;
	};
	class Loftd_akito_bag: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_blacksuitshirt: Clothing
	{
		itemsCargoSize[]={2,1};
	};
	class Loftd_blacksuitpants: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={5,1};
		notOutside=1;
	};
	class Loftd_Michonneshirt_ColorBase: Clothing
	{
		itemsCargoSize[]={4,1};
	};
	class Loftd_Michonnepants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
		notOutside=1;
	};
	class Loftd_MiniDress_ColorBase: Clothing
	{
		itemsCargoSize[]={2,1};

	};
	class Loftd_Sheriffshirt_ColorBase: Clothing
	{
		itemsCargoSize[]={4,1};
	};
	class Loftd_Sheriffpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
		notOutside=1;
	};
	class Loftd_wdpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_wdcoat_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
		varWetMax=0.249;
	};

	class Loftd_canadajacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_hoodie_ColorBase: Clothing
	{
		itemsCargoSize[]={4,1};
	};
	class Loftd_plumpjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_pufferjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_sleevelessshirt_ColorBase: Clothing
	{
		itemsCargoSize[]={4,1};
	};
	class Loftd_Stylishjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={7,1};
	};

	class Loftd_dixonvest_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_Puffervest_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};

	class Loftd_coloradopants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_coloradopants_bl: Loftd_coloradopants_ColorBase
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_coloradopants_G63: Loftd_coloradopants_ColorBase
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_Ironsightpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_Ironsightpantsv2_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={6,1};
	};
	class Loftd_Ironsightpantsv3_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_militarysetpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={2,1};
	};
	class Loftd_tacticalpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};

	class Loftd_CHRSAT_jacket: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={4,1};
	};
	class Loftd_CHRSAT_pants: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_WILD_jacket: Loftd_CHRSAT_jacket
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={3,1};
	};
	class Loftd_WILD_pants: Loftd_CHRSAT_pants
	{
		itemsCargoSize[]={8,1};
	};
	class Loftd_dawnpants_ColorBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemsCargoSize[]={7,1};
	};
	class Loftd_dawnjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={4,1};
	};

	class Loftd_coloradojacket: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_coloradojacket_dark: Loftd_coloradojacket
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_coloradojacket_G63: Loftd_coloradojacket
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_coloradojacket_camogr: Loftd_coloradojacket
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_coloradojacket_camogr2: Loftd_coloradojacket
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};
	class Loftd_Ironsightjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_Ironsightshirt_ColorBase: Clothing
	{
		itemsCargoSize[]={4,1};
	};
	class Loftd_Ironsightwindbreaker_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={5,1};
	};
	class Loftd_militarysetjacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={3,1};
	};
	class Loftd_tacticaljacket_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemsCargoSize[]={8,1};
	};

	class Loftd_kronboots_ColorBase: Clothing
	{
		attachments[]=
		{
			"Knife"
		};
	};
	class Loftd_vagabondboots_ColorBase: Clothing
	{
		attachments[]=
		{
			"Knife"
		};
		itemSize[]={3,3};
	};

	class Loftd_militarysetvest_ColorBase: Clothing
	{
		bulletProofProtection = 0.3;
		itemsCargoSize[]={4,2};
	};
	class Loftd_militarySvest_ColorBase : Clothing
    {
    	bulletProofProtection = 0.4;
    };
    class Loftd_militaryvestCIRAS_ColorBas : Clothing
    {
    	bulletProofProtection = 0.35;
    };

	class Loftd_bigpouch: Container_Base
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_bigpouch2: Container_Base
	{
		itemsCargoSize[]={2,3};
	};
	class Loftd_sidebag1: Container_Base
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_sidebag2: Container_Base
	{
		itemsCargoSize[]={2,3};
	};
	class Loftd_Middletopbag: Container_Base
	{
		itemsCargoSize[]={3,2};
	};

	class Loftd_cloak_ColorBase: Clothing
	{
		attachments[] =
		{
			"Pistol"
		};
	};

	class Loftd_dragonkatana: Machete
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};
	class Loftd_michonnekatana: Machete
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};
	class Loftd_sakurakatana: Machete
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};

	class Loftd_roughsword: Machete
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2000;
				};
			};
		};
	};

	class Loftd_tacticalaxe: Hatchet
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 2500;
				};
			};
		};
	};

	class Loftd_wallet : Container_Base
    {
    	itemsCargoSize[]={4, 2};
    	repairableWithKits[]={3};
		repairCosts[]={25};
    	inventorySlot[]={"Wallet"};
    };

//Apokot
// class apokot_wellies_ColorBase : Clothing
//     {
//         displayName="$STR_CfgVehicles_Wellies_ColorBase0";
//         descriptionShort="A type of footwear originally designed to protect feet from water, moisture and other environmental factors.";
//         model="\DZM-Pack\mods\wellies\g\wellies_g.p3d";
//         inventorySlot[]+={"Feet"};
//         itemInfo[]={"Clothing", "Feet"};
//         itemSize[]={3, 3};
//         weight=1100;
//         durability=0.5;
//         repairableWithKits[]={5};
//         repairCosts[]={30};
//         varWetMax=0.249;
//         heatIsolation=0.4;
//         soundAttType="Boots";
//         hiddenSelections[]={"camoground", "camomale", "camofemale"};
//         attachments[]=
// 		{
// 			"Knife"
// 		};
// 		radiationProtection = 0.95;

//         class ClothingTypes
//         {
//             male="\DZM-Pack\mods\wellies\wellies_m.p3d";
//             female="\DZM-Pack\mods\wellies\wellies_m.p3d";
//         };
//         class DamageSystem
//         {
//             class GlobalHealth
//             {
//                 class Health
//                 {
//                     hitpoints=100;
//                     healthLevels[]={{1, {"DZM-Pack\mods\wellies\data\wellies.rvmat"}}, {0.7, {"DZM-Pack\mods\wellies\data\wellies.rvmat"}}, {0.5, {"DZM-Pack\mods\wellies\data\wellies_damage.rvmat"}}, {0.3, {"DZM-Pack\mods\wellies\data\wellies_damage.rvmat"}}, {0, {"DZM-Pack\mods\wellies\data\wellies_destruct.rvmat"}}};
//                 };
//             };
//             class GlobalArmor
//             {
//                 class Melee
//                 {
//                     class Health
//                     {
//                         damage=1;
//                     };
//                     class Blood
//                     {
//                         damage=0.9;
//                     };
//                     class Shock
//                     {
//                         damage=1;
//                     };
//                 };
//                 class Infected
//                 {
//                     class Health
//                     {
//                         damage=1;
//                     };
//                     class Blood
//                     {
//                         damage=0.9;
//                     };
//                     class Shock
//                     {
//                         damage=1;
//                     };
//                 };
//             };
//         };
//         class Protection
//         {
//             biological=0;
//             chemical=0.5;
//         };
//         class AnimEvents
//         {
//             class SoundWeapon
//             {
//                 class pickUpItem
//                 {
//                     soundSet="AthleticShoes_pickup_SoundSet";
//                     id=797;
//                 };
//                 class drop
//                 {
//                     soundset="AthleticShoes_drop_SoundSet";
//                     id=898;
//                 };
//             };
//         };
//     };
//     class apokot_wellies_green : apokot_wellies_ColorBase
//     {
//         scope=2;
//         hiddenSelectionsTextures[]={"\DZM-Pack\mods\wellies\data\wellies_green_co.paa", "\DZM-Pack\mods\wellies\data\wellies_green_co.paa", "\DZM-Pack\mods\wellies\data\wellies_green_co.paa"};
//     };
//     class apokot_wellies_black : apokot_wellies_ColorBase
//     {
//         scope=2;
//         hiddenSelectionsTextures[]={"\DZM-Pack\mods\wellies\data\wellies_black_co.paa", "\DZM-Pack\mods\wellies\data\wellies_black_co.paa", "\DZM-Pack\mods\wellies\data\wellies_black_co.paa"};
//     };

//Project Hope
    class PH_Backpack_01_ColorBase : Clothing
	{
		attachments[]+=
		{
			"mapbag",
			"Compass",
			"Paper",
			"Pen",
			"Wallet"
		};
		itemsCargoSize[]={7,8};
	};

////Dayz Bicycle
//
//	class CarScript;
//	class Bicycle_Base: CarScript
//	{
//		class SimulationModule
//		{
//			class Steering // better and more responsive steering for beeter crash avoidance. It should be more maneuverable than a truck in my opinion.
//			{
//			    maxSteeringAngle=30;    //35 
//			    increaseSpeed[]={0,30,10,25,40,20}; // {0,25,50,15} <- was literally same as V3S truck
//			    decreaseSpeed[]={0,65,35,45}; // {0,50,50,40}
//			    centeringSpeed[]={0,25,40,15}; // {0,25,50,15}
//			};
//
//			class Engine // smoother, less car like acceleration 
//			{
//			    torqueCurve[]={650,0,750,60,1000,100,2150,145,3900,125,7400,0};
//			    inertia=0.1;            //0.44999999 <- Inertia means Rotating engine mass which is almost none for bicycles
//			    frictionTorque=65;        // 165 <- way to high
//			    rollingFriction=0.0;        // 2 <- useless
//			    viscousFriction=0.05;        // 1.25 <- now you can go down hill in first and pick up speed instead of breaking the sound barrier in neutral.
//			    rpmIdle=850;
//			    rpmMin=900;
//			    rpmClutch=1350;
//			    rpmRedline=50000;        // 7300 <- safety change
//			};
//
//			class Gearbox
//			{
//			    type="GEARBOX_MANUAL";
//			    reverse=3.7;  // <-- new top speed 31km/h instead of 44km/h
//			    ratios[]={3.11};  // <-- new top speed 37km/h instead of 31km/h
//			};
//		};
//	};
//	class Lady_Bicycle : Bicycle_Base
//	{
//		class SimulationModule
//		{
//			class Steering // better and more responsive steering for beeter crash avoidance. It should be more maneuverable than a truck in my opinion.
//			{
//			    maxSteeringAngle=30;    //35 
//			    increaseSpeed[]={0,30,10,25,40,20}; // {0,25,50,15} <- was literally same as V3S truck
//			    decreaseSpeed[]={0,65,35,45}; // {0,50,50,40}
//			    centeringSpeed[]={0,25,40,15}; // {0,25,50,15}
//			};
//
//			class Engine // smoother, less car like acceleration 
//			{
//			    torqueCurve[]={650,0,750,60,1000,100,2150,145,3900,125,7400,0};
//			    inertia=0.1;            //0.44999999 <- Inertia means Rotating engine mass which is almost none for bicycles
//			    frictionTorque=65;        // 165 <- way to high
//			    rollingFriction=0.0;        // 2 <- useless
//			    viscousFriction=0.05;        // 1.25 <- now you can go down hill in first and pick up speed instead of breaking the sound barrier in neutral.
//			    rpmIdle=850;
//			    rpmMin=900;
//			    rpmClutch=1350;
//			    rpmRedline=50000;        // 7300 <- safety change
//			};
//
//			class Gearbox
//			{
//			    type="GEARBOX_MANUAL";
//			    reverse=3.7;  // <-- new top speed 31km/h instead of 44km/h
//			    ratios[]={3.11};  // <-- new top speed 37km/h instead of 31km/h
//			};
//		};
//	};
//
////Underground Bases
//	class BaseBuildingBase;
//	class UndergroundHatch : BaseBuildingBase
//	{
//		attachments[] = { "CamoNet", "Material_WoodenPlanks", "Material_L1_WoodenPlanks", "Material_Nails", "Material_FPole_WoodenLog2" };
//		class GUIInventoryAttachmentsProps
//        {
//            class Dirt
//            {
//                name="Hatch";
//                description="";
//                attachmentSlots[]={"Material_Nails", "Material_WoodenPlanks", "Material_L1_WoodenPlanks", "Material_FPole_WoodenLog2"};
//                icon="set:dayz_inventory image:tf_stones";
//                selection="wall";
//            };
//            class Locks
//            {
//            	name="";
//                description="";
//                attachmentSlots[]={};
//                icon="";
//                selection="";
//            };
//            class Attachments
//            {
//                name="$STR_attachment_accessories";
//                description="";
//                attachmentSlots[]={"CamoNet"};
//                icon="set:dayz_inventory image:cat_fp_tents";
//                view_index=1;
//            };
//        };
//	};
//
//Expansion
	class Expansion_Kar98_Bayonet : AK_Bayonet
	{
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 1500;
				};
			};
		};
	};

	class ExpansionHelicopterBattery : Inventory_Base
	{
		class EnergyManager
		{
			energyStorageMax=4000;
			energyAtSpawn=4000;
		};
	};
//Namalsk
	// class BubbleGoose_ColorBase: Clothing
	// {
	// 	itemsCargoSize[]={4,2};
	// };
};
