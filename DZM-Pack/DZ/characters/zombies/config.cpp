class CfgPatches
{
	class DZ_Characters_Zombies
	{
		units[]=
		{
			"Hermit_NewAI"
		};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"DZ_Characters"
		};
	};
};
class CfgVehicles
{
	class DZ_LightAI;
	class DayZInfected: DZ_LightAI
	{
	};
	class ZombieBase: DayZInfected
	{
		class Cargo
		{
			itemsCargoSize[]={8,8};
			allowOwnedCargoManipulation=1;
			openable=0;
		};
	};
};