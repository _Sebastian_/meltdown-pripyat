modded class Environment
{
	const float WATER_LEVEL_LOW 	= 0.1;//mod 0.5
	const float WATER_LEVEL_NONE	= 0.0;//mod 0.15
//mod<
	const float groundWetRain = 1.0 / 300;
	const float groundDryTemp = 1.0 / 3600;
	const float groundDryWind = 1.0 / 1800;

	protected ref array<int> 		m_SlotIdsNoLegs;
	protected ref array<int> 		m_SlotHead;
	protected ref array<int> 		m_SlotHeadRest;
	protected ref array<int> 		m_SlotBody;
	protected ref array<int> 		m_SlotBodyRest;
	protected ref array<int> 		m_SlotLegs;
	
	protected float m_GroundWetness;
	protected bool m_IsOnWetGround;
	protected int coverRainTick;
//mod>
	
	override void Init(PlayerBase pPlayer)
	{
//mod<
		m_GroundWetness					= 0.0;
		m_IsOnWetGround					= false;
		coverRainTick 					= 0;
//mod>

//mod<
		m_SlotIdsNoLegs			= new array<int>();
		m_SlotIdsNoLegs 		= {
			InventorySlots.HEADGEAR,
			InventorySlots.MASK,
			InventorySlots.EYEWEAR,
			InventorySlots.GLOVES,
			InventorySlots.ARMBAND,
			InventorySlots.BODY,
			InventorySlots.HIPS,
			InventorySlots.VEST,
			InventorySlots.BACK,
			InventorySlots.FEET
		};
		
		m_SlotHead				= new array<int>();
		m_SlotHead				= {
			InventorySlots.HEADGEAR
		};
		
		m_SlotHeadRest			= new array<int>();
		m_SlotHeadRest			= {
			InventorySlots.EYEWEAR,
			InventorySlots.MASK
		};
		
		m_SlotBody				= new array<int>();
		m_SlotBody				= {
			InventorySlots.BODY,
			InventorySlots.ARMBAND,
			InventorySlots.HIPS,
			InventorySlots.VEST,
			InventorySlots.BACK
		};
		
		m_SlotBodyRest			= new array<int>();
		m_SlotBodyRest			= {
			InventorySlots.GLOVES
		};
		
		m_SlotLegs				= new array<int>();
		m_SlotLegs 				= {
			InventorySlots.LEGS
		};
//mod>
	}
	

	// Calculates heatisolation of clothing, process its wetness, collects heat from heated items and calculates player's heat comfort
	override void Update(float pDelta)
	{
		// Print("playerWet 0");
		if (m_Player && m_Initialized)
		{
			m_RoofCheckTimer += pDelta;
			//! check if player is under roof (only if the Building check is false)
			if ( m_RoofCheckTimer >= GameConstants.ENVIRO_TICK_ROOF_RC_CHECK )
			{
				if ( !IsInsideBuilding() )
					CheckUnderRoof();
				
				m_RoofCheckTimer = 0;
			}

			m_Time += pDelta;
			if (m_Time >= GameConstants.ENVIRO_TICK_RATE)
			{
				m_Time = 0;
				m_WetDryTick++; // Sets whether it is time to add wetness to items and clothing

				//! Updates data
				CheckWaterContact(m_WaterLevel);//under 0.25 is 0 for some reason
				CollectAndSetPlayerData();
				CollectAndSetEnvironmentData();
				GatherTemperatureSources();

				ProcessTemperatureSources();
				
				//! Process temperatures
				ProcessItemsTemperature(m_HeadParts);
				ProcessItemsTemperature(m_BodyParts);
				ProcessItemsTemperature(m_FeetParts);
				ProcessItemsInHandsTemperature();

				//! heat comfort calculation
				if (DetermineHeatcomfortBehavior())
					SetHeatcomfortDirectly();
				else
					ProcessHeatComfort();

				//! Process item wetness/dryness
				if (m_WetDryTick >= GameConstants.ENVIRO_TICKS_TO_WETNESS_CALCULATION)
				{
//mod<
					// Print("playerWet 1");
					// float wetChange = 0;
					// if (IsRaining())
					// {
					// 	wetChange = Math.Sqrt(m_Rain) * groundWetRain;
					// }
					// else
					// {
					// 	wetChange = Math.Sqrt(Math.AbsFloat(m_EnvironmentTemperature) + 1) * groundDryTemp;
					// 	wetChange += Math.Sqrt(m_Wind + 1) * groundDryWind;
					// 	wetChange *= 1 - (m_Clouds + m_Fog) * 0.25;
					// 	wetChange *= -1;
					// }
					// m_GroundWetness = Math.Clamp(m_GroundWetness + wetChange * GameConstants.ENVIRO_TICK_RATE, 0, 1);

					m_GroundWetness = (m_Clouds + m_Fog) * 0.5;
					m_IsOnWetGround = m_GroundWetness > 0 && (m_SurfaceType.Contains("grass") || m_SurfaceType.Contains("dirt") || m_SurfaceType.Contains("broadleaf")|| m_SurfaceType.Contains("conifer"));
					//Print("waterLevel:" + m_WaterLevel + " groundWet:" + m_GroundWetness + " groundType: " + m_SurfaceType + " isWet:" + m_IsOnWetGround + " temp:" + m_EnvironmentTemperature + " wind:" + m_Wind);


					bool feetWet = false;
					EntityAI feetItem = m_Player.GetItemOnSlot("Feet");
					if (feetItem)
					{
						feetWet = feetItem.GetWet() >= GameConstants.STATE_WET;
					}

					EntityAI legsItem = m_Player.GetItemOnSlot("Legs");
					bool pantsOutside = legsItem && !legsItem.ConfigGetBool("notOutside");
					//Print("pantsOutside " + pantsOutside);

					HumanMovementState hms = new HumanMovementState();
					m_Player.GetMovementState(hms);
					if (IsWaterContact())
					{
						float waterLevelShoes = WATER_LEVEL_LOW;
						if (feetItem && !pantsOutside)
						{
							int size_x, size_y;
							GetGame().GetInventoryItemSize(feetItem, size_x, size_y);
							waterLevelShoes = size_y * 0.1;
						}
						
						if (m_WaterLevel < waterLevelShoes)
						{
							if (hms.m_iStanceIdx == DayZPlayerConstants.STANCEIDX_ERECT)
							{
								ProcessItemsWetness(m_SlotIdsLower);
							}
							else
							{
								ProcessItemsWetness(m_FeetParts);
							}
						}
						else if (m_WaterLevel < WATER_LEVEL_MID)
						{
							if (hms.m_iStanceIdx == DayZPlayerConstants.STANCEIDX_ERECT)
							{
								ProcessItemsWetness(m_SlotIdsBottom);
							}
							else
							{
								ProcessItemsWetness(m_SlotIdsUpper);
							}
						}
						else if (m_WaterLevel < WATER_LEVEL_HIGH)
						{
							ProcessItemsWetness(m_SlotIdsUpper);
						}
						else
						{
							ProcessItemsWetness(m_SlotIdsComplete);
						}
					}
					else if ((m_IsOnWetGround || IsRaining() || (IsSnowing() && MiscGameplayFunctions.GetCombinedSnowfallWindValue() > SNOWFALL_WIND_COMBINED_THRESHOLD)) && !IsInsideBuilding() && !IsUnderRoof() && !IsInsideVehicle())
					{
						if (m_IsOnWetGround)
						{
							if (hms.IsInProne())
							{
								//Print("in prone");
								ProcessItemsWetness(m_SlotIdsUpper);
							}
							else
							{
								//Print("standing");
								ProcessItemsWetness(m_SlotIdsLower);
								if ((!feetItem || feetWet || pantsOutside) && m_SurfaceType.Contains("grass"))
								{
									//Print("wet feet");
									ProcessItemsWetness(m_SlotLegs);
								}
							}
						}
						else
						{
							ProcessItemsWetness(m_SlotHead);
							bool headWet = false;
							EntityAI headItem = m_Player.GetItemOnSlot("Headgear");
							if (headItem)
							{
								headWet = headItem.GetWet() >= GameConstants.STATE_WET;
							}
							if (!headItem || headWet || coverRainTick == 0)
							{
								ProcessItemsWetness(m_SlotHeadRest);
							}

							ProcessItemsWetness(m_SlotBody);
							bool bodyWet = false;
							EntityAI bodyItem = m_Player.GetItemOnSlot("Body");
							if (bodyItem)
							{
								bodyWet = bodyItem.GetWet() >= GameConstants.STATE_WET;
							}
							if (!bodyItem || bodyWet || coverRainTick == 0)
							{
								ProcessItemsWetness(m_SlotBodyRest);
							}

							ProcessItemsWetness(m_SlotIdsLower);
							if (!bodyItem || bodyWet || !feetItem || feetWet || pantsOutside || coverRainTick == 0)
							{
								ProcessItemsWetness(m_SlotLegs);
							}

							// Print("Raintick:" + coverRainTick);
							coverRainTick++;
							if (coverRainTick == 16)
							{
								coverRainTick = 0;
							}
						}
					}
//mod>
					else
					{
						ProcessItemsDryness();
					}
					
					// Print("Head:" + headWet + " Body:" + bodyWet + " Feet:" + feetWet);

					//! setting of wetness/dryiness of player
					if ((m_ItemsWetnessMax < GameConstants.STATE_WET) && (m_Player.GetStatWet().Get() == 1))
					{
						m_Player.GetStatWet().Set(0);
					}
					else if ((m_ItemsWetnessMax >= GameConstants.STATE_WET) && (m_Player.GetStatWet().Get() == 0))
					{
						m_Player.GetStatWet().Set(1);
					}

					m_WetDryTick = 0;
					m_ItemsWetnessMax = 0; //! reset item wetness counter;
				}
			}
		}
	}


// 	// Calculats wet/drying delta based on player's location and weather
	override float GetWetDelta()
	{
		float wetDelta = 0;
		if ( IsWaterContact() )
		{
//mod<
			if (m_WaterLevel < WATER_LEVEL_LOW)
			{
				wetDelta = 0.25;
			}
			else if (m_WaterLevel < WATER_LEVEL_MID)
			{
				wetDelta = 0.5;
			}
			else if (m_WaterLevel < WATER_LEVEL_HIGH)
			{
				wetDelta = 0.75;
			}
			else
			{
				wetDelta = 1;
			}
		}
		else if ((!IsInsideBuilding() && !IsUnderRoof() && !IsInsideVehicle())
		{
			if (IsRaining())
				wetDelta = GameConstants.ENVIRO_WET_INCREMENT * GameConstants.ENVIRO_TICKS_TO_WETNESS_CALCULATION * (m_Rain) * (1 + (GameConstants.ENVIRO_WIND_EFFECT * m_Wind));

			if (IsSnowing() && MiscGameplayFunctions.GetCombinedSnowfallWindValue() > SNOWFALL_WIND_COMBINED_THRESHOLD)
				wetDelta = GameConstants.ENVIRO_WET_INCREMENT * GameConstants.ENVIRO_TICKS_TO_WETNESS_CALCULATION * (m_Snowfall - SNOWFALL_LIMIT_LOW) * GameConstants.ENVIRO_SNOW_WET_COEF * (1 + (GameConstants.ENVIRO_WIND_EFFECT * m_Wind));

			if (m_IsOnWetGround && !IsRaining() && !IsSnowing())
				wetDelta = 0.03125 * m_GroundWetness * (m_PlayerSpeed + 1);
		}
//mod>
		else
		{
			//! player is drying
			float tempEffect = Math.Max(m_PlayerHeat + GetEnvironmentTemperature(), 1.0);

			float weatherEffect = ((1 - (m_Fog * GameConstants.ENVIRO_FOG_DRY_EFFECT))) * (1 - (m_Clouds * GameConstants.ENVIRO_CLOUD_DRY_EFFECT));
			if (weatherEffect <= 0)
			{
				weatherEffect = 1.0;
			}
			
			wetDelta = -(GameConstants.ENVIRO_DRY_INCREMENT * weatherEffect * tempEffect);
			if (!IsInsideBuilding())
			{
				wetDelta *= 1 + (GameConstants.ENVIRO_WIND_EFFECT * m_Wind);
			}
		}

		// Print("playerWet:" + wetDelta);
		return wetDelta;
	}
	
// // Calculates and return temperature of environment
// 	override protected float GetEnvironmentTemperature()
// 	{
// 		float temperature;
// 		temperature = g_Game.GetMission().GetWorldData().GetBaseEnvTemperature();
// 		temperature += Math.AbsFloat(temperature * m_Clouds * GameConstants.ENVIRO_CLOUDS_TEMP_EFFECT);

// 		if (IsWaterContact())
// 		{
// 			temperature -= Math.AbsFloat(temperature * GameConstants.ENVIRO_WATER_TEMPERATURE_COEF) * m_WaterLevel * m_WaterLevel;//mod was without water level
// 		}
		
// 		if (IsInsideBuilding() || m_IsUnderRoofBuilding)
// 		{
// 			temperature += Math.AbsFloat(temperature * GameConstants.ENVIRO_TEMPERATURE_INSIDE_COEF);
// 		}
// 		else if (IsInsideVehicle())
// 		{
// 			temperature += Math.AbsFloat(temperature * GameConstants.ENVIRO_TEMPERATURE_INSIDE_VEHICLE_COEF);
// 		}
// 		else if (IsUnderRoof() && !m_IsUnderRoofBuilding)
// 		{
// 			temperature += Math.AbsFloat(temperature * GameConstants.ENVIRO_TEMPERATURE_UNDERROOF_COEF);
// 			temperature -= GameConstants.ENVIRO_TEMPERATURE_WIND_COEF * GetWindModifierPerSurface() * m_Wind;
// 		}
// 		else
// 		{
// 			temperature -= GameConstants.ENVIRO_TEMPERATURE_WIND_COEF * GetWindModifierPerSurface() * m_Wind;
// 			temperature -= Math.AbsFloat(temperature * m_Fog * GameConstants.ENVIRO_FOG_TEMP_EFFECT);
// 			temperature -= GetTemperatureHeightCorrection();
// 		}
		
// 		// incorporate temperature from temperature sources (buffer)
// 		if (Math.AbsFloat(m_UTSAverageTemperature) > 0.001)
// 		{
// 			temperature += m_UTSAverageTemperature;
// 		}
		
// 		return temperature;
// 	}

// 	//! returns weighted avg heat comfort for bodypart
// 	override protected void BodyPartHeatProperties(array<int> pBodyPartIds, float pCoef, out float pHeatComfort, out float pHeat)
// 	{
// 		int attCount;
		
// 		EntityAI attachment;
// 		ItemBase item;
		
// 		pHeatComfort = -1;
// 		attCount = m_Player.GetInventory().AttachmentCount();
		
// 		for (int attIdx = 0; attIdx < attCount; attIdx++)
// 		{
// 			attachment = m_Player.GetInventory().GetAttachmentFromIndex(attIdx);
// 			if (attachment.IsClothing())
// 			{
// 				item = ItemBase.Cast(attachment);
// 				int attachmentSlot = attachment.GetInventory().GetSlotId(0);

// 				//! go through all body parts we've defined for that zone (ex.: head, body, feet)
// 				for (int i = 0; i < pBodyPartIds.Count(); i++)
// 				{
// 					if (attachmentSlot == pBodyPartIds.Get(i))
// 					{
// 						float heatIsoMult = 1.0;
// 						if (attachmentSlot == InventorySlots.VEST)
// 						{
// 							heatIsoMult = GameConstants.ENVIRO_HEATISOLATION_VEST_WEIGHT;
// 						}

// 						if (attachmentSlot == InventorySlots.BACK)
// 						{
// 							heatIsoMult = GameConstants.ENVIRO_HEATISOLATION_BACK_WEIGHT;
// 						}

// 						pHeatComfort += heatIsoMult * MiscGameplayFunctions.GetCurrentItemHeatIsolation(item);
						
// 						// go through any attachments and cargo (only current level, ignore nested containers - they isolate)
// 						int inAttCount = item.GetInventory().AttachmentCount();
// 						if (inAttCount > 0)
// 						{
// 							for (int inAttIdx = 0; inAttIdx < inAttCount; inAttIdx++)
// 							{
// 								EntityAI inAttachment = item.GetInventory().GetAttachmentFromIndex(inAttIdx);
// 								ItemBase itemAtt = ItemBase.Cast(inAttachment);
// 								if (itemAtt)
// 								{
// 									pHeat += itemAtt.GetTemperature();
// //mod< check for inner clothing layer
// 									if (attachmentSlot == InventorySlots.BODY || attachmentSlot == InventorySlots.LEGS || attachmentSlot == InventorySlots.FEET)
// 									{
// 										pHeatComfort += heatIsoMult * MiscGameplayFunctions.GetCurrentItemHeatIsolation(itemAtt);
// 									}
// //mod>
// 								}
// 							}
// 						}
// 						if (item.GetInventory().GetCargo())
// 						{
// 							int inItemCount = item.GetInventory().GetCargo().GetItemCount();
							
// 							for (int j = 0; j < inItemCount; j++)
// 							{
// 								ItemBase inItem;
// 								if (Class.CastTo(inItem, item.GetInventory().GetCargo().GetItem(j)))
// 								{
// 									pHeat += inItem.GetTemperature();
// 								}
// 							}
// 						}
// 					}
// 				}
// 			}
// 		}

// 		pHeatComfort = (pHeatComfort / pBodyPartIds.Count()) * pCoef;
// 		pHeat = (pHeat / pBodyPartIds.Count()) * pCoef;
// 	}
}
