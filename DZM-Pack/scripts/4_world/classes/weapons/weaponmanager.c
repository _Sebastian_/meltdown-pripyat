modded class WeaponManager
{
	override Magazine GetPreparedMagazine()
	{
		super.GetPreparedMagazine();

		for (int i = 0; i < m_SuitableMagazines.Count(); i++)
		{
			Clothing parentClothing = Clothing.Cast(m_SuitableMagazines.Get(i).GetHierarchyParent());

			if (parentClothing && parentClothing.!CanAccess())
			{
				m_SuitableMagazines.Remove(i);
				i--;
				continue;
			}
			if (m_SuitableMagazines.Get(i).GetAmmoCount() > 0)
				return m_SuitableMagazines.Get(i);
		}

		return null;
	}
}