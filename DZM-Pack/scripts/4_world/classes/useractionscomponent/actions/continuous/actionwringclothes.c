modded class ActionWringClothes: ActionContinuousBase
{
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if (player.IsInWater()) return false;
		
		//! wet+ items (so they will stay damp after wringing)
		if ( item && item.GetWet() >= GameConstants.STATE_WET && item.IsEmpty())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
};