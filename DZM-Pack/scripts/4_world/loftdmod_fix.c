modded class Loftd_wallet extends Container_Base
{
	ref array<string> m_AllowedCargo = { 
		"Loftd_money", 
		"Loftd_crescent", 
		"Loftd_FairyEarrings",
		"Loftd_naturalstones",
		"Loftd_ddring",
		"Loftd_goldring",
		"Loftd_oldring",
		"Loftd_skullring",
		"Loftd_vikingring",

		"alp_Cash", 
		"Money_Base", 
		"CJ_Money_Base", 
		"CJ_Coin_Base", 

		"ChernarusMap",
		"Paper", 
		"PetrolLighter", 
		"GlowPlug",
		"SparkPlug",
		"Matchbox",
		"Worm",
		"Hook", 
		"BoneHook",
		"Bait",
		"BoneBait",
		"ChickenFeather", 
		"CanOpener",
		"Lockpick",
		"Whetstone",
		"Battery9V",
		"BrassKnuckles_ColorBase",
		"Handcuffs",
		"HandcuffKeys",
		"NioshFaceMask",
		"SurgicalMask",
		"SurgicalGloves_ColorBase",
		"Thermometer",
		"BloodTestKit",
		"BloodBagEmpty",
		"PurificationTablets",
		"TomatoSeeds",
		"PepperSeeds",
		"PumpkinSeeds",
		"ZucchiniSeeds",
		"CannabisSeeds",
		"TomatoSeedsPack",
		"PepperSeedsPack",
		"PumpkinSeedsPack",
		"ZucchiniSeedsPack",
		"CannabisSeedsPack",
		"Zagorky_ColorBase",

		"TabletsBase",

		"Pen_ColorBase"
	};

	//override bool CanReceiveItemIntoCargo(EntityAI item)
	//{
	//	if (!super.CanReceiveItemIntoCargo(item))
	//	{
	//		return false;
	//	}

	//	foreach(string allowedCargo : m_AllowedCargo)
	//	{
	//		if (item.IsKindOf(allowedCargo))
	//		{
	//			return true;
	//		}
	//	}

	//	return false;
	//}

	//override bool CanSwapItemInCargo(EntityAI child_entity, EntityAI new_entity)
	//{
	//	if (!super.CanSwapItemInCargo(child_entity, new_entity))
	//	{
	//		return false;
	//	}

	//	foreach(string allowedCargo : m_AllowedCargo)
	//	{
	//		if (new_entity.IsKindOf(allowedCargo))
	//		{
	//			return true;
	//		}
	//	}

	//	return false;
	//}
}



modded class Loftd_kneepadspants_ColorBase extends Pants_Base
{
	override void EEInit()
	{
		super.EEInit();
		HideUnhideSelection("fullpants", 0);
		HideUnhideSelection("bootspants", 1);
	}
	ref array<typename> m_Exceptionshigh =
	{
		JungleBoots_ColorBase, MilitaryBoots_ColorBase, CombatBoots_ColorBase, TTSKOBoots, Wellies_ColorBase, MedievalBoots, Loftd_IronsightBoots_ColorBase, Loftd_vagabondboots_ColorBase, Loftd_kronboots_ColorBase, Loftd_blacksuitboots, Kirza_Boots_ColorBase, High_Knee_Sneakers
	};
	//override void SetActions()
	//{
	//	super.SetActions();
	//	AddAction(ActionWringClothes);
	//}
	//void HideUnhideSelection(string selectionName, bool hide = false)
	//{
	//	TStringArray selectionNames = new TStringArray;
	//	ConfigGetTextArray("simpleHiddenSelections", selectionNames);
	//	int selectionId = selectionNames.Find(selectionName);
	//	SetSimpleHiddenSelectionState(selectionId, hide);
	//}
};

modded class Loftd_blackpants_ColorBase extends Pants_Base
{
	override void EEInit()
	{
		super.EEInit();
		HideUnhideSelection("freepants", 0);
		HideUnhideSelection("junglepants", 0);
		HideUnhideSelection("hikingpants", 0);
		HideUnhideSelection("welliesboots", 1);
	}
	ref array<typename> m_Exceptionshiking =
	{
		HikingBoots_ColorBase, WorkingBoots_ColorBase, Loftd_coloradoboots_ColorBase, Loftd_combatboots_ColorBase, Loftd_wolverineBoots_ColorBase, Loftd_CHRSAT_boots, Loftd_akito_shoes_ColorBase
	};
	ref array<typename> m_Exceptionsjungle =
	{
		JungleBoots_ColorBase, MilitaryBoots_ColorBase, CombatBoots_ColorBase, TTSKOBoots, Loftd_IronsightBoots_ColorBase, Loftd_vagabondboots_ColorBase, Loftd_kronboots_ColorBase, Loftd_blacksuitboots
	};
	ref array<typename> m_Exceptionswellies =
	{
		Wellies_ColorBase, MedievalBoots, Kirza_Boots_ColorBase, High_Knee_Sneakers
	};
	//override void SetActions()
	//{
	//	super.SetActions();
	//	AddAction(ActionWringClothes);
	//}
	//void HideUnhideSelection(string selectionName, bool hide = false)
	//{
	//	TStringArray selectionNames = new TStringArray;
	//	ConfigGetTextArray("simpleHiddenSelections", selectionNames);
	//	int selectionId = selectionNames.Find(selectionName);
	//	SetSimpleHiddenSelectionState(selectionId, hide);
	//}
};

modded class Loftd_akito_pants_ColorBase extends Pants_Base
{
	override void EEInit()
	{
		super.EEInit();
		HideUnhideSelection("hidestraps", 0);
		HideUnhideSelection("junglestraps", 0);
	}
	ref array<typename> m_Exceptionsjungle =
	{
		JungleBoots_ColorBase, MilitaryBoots_ColorBase, CombatBoots_ColorBase, TTSKOBoots, Loftd_IronsightBoots_ColorBase, Loftd_vagabondboots_ColorBase, Loftd_kronboots_ColorBase, Loftd_blacksuitboots
	};
	ref array<typename> m_Exceptionswellies =
	{
		Wellies_ColorBase, MedievalBoots, Kirza_Boots_ColorBase, High_Knee_Sneakers
	};

	//override void SetActions()
	//{
	//	super.SetActions();
	//	AddAction(ActionWringClothes);
	//}
	//void HideUnhideSelection(string selectionName, bool hide = false)
	//{
	//	TStringArray selectionNames = new TStringArray;
	//	ConfigGetTextArray("simpleHiddenSelections", selectionNames);
	//	int selectionId = selectionNames.Find(selectionName);
	//	SetSimpleHiddenSelectionState(selectionId, hide);
	//}
};

modded class Loftd_jeanspants_ColorBase extends Pants_Base
{
	override void EEInit()
	{
		super.EEInit();
		HideUnhideSelection("freepants", 0);
		HideUnhideSelection("junglepants", 0);
		HideUnhideSelection("hikingpants", 0);
		HideUnhideSelection("welliesboots", 1);
	}
	ref array<typename> m_Exceptionshiking =
	{
		HikingBoots_ColorBase, WorkingBoots_ColorBase, Loftd_coloradoboots_ColorBase, Loftd_combatboots_ColorBase, Loftd_wolverineBoots_ColorBase, Loftd_CHRSAT_boots, Loftd_akito_shoes_ColorBase
	};
	ref array<typename> m_Exceptionsjungle =
	{
		JungleBoots_ColorBase, MilitaryBoots_ColorBase, CombatBoots_ColorBase, TTSKOBoots, Loftd_IronsightBoots_ColorBase, Loftd_vagabondboots_ColorBase, Loftd_kronboots_ColorBase, Loftd_blacksuitboots
	};
	ref array<typename> m_Exceptionswellies =
	{
		Wellies_ColorBase, MedievalBoots, Kirza_Boots_ColorBase, High_Knee_Sneakers
	};
	//override void SetActions()
	//{
	//	super.SetActions();
	//	AddAction(ActionWringClothes);
	//}
	//void HideUnhideSelection(string selectionName, bool hide = false)
	//{
	//	TStringArray selectionNames = new TStringArray;
	//	ConfigGetTextArray("simpleHiddenSelections", selectionNames);
	//	int selectionId = selectionNames.Find(selectionName);
	//	SetSimpleHiddenSelectionState(selectionId, hide);
	//}
};

modded class Loftd_Michonnepants_ColorBase extends Pants_Base
{
	override void EEInit()
	{
		super.EEInit();
		HideUnhideSelection("freepants", 0);
		HideUnhideSelection("junglepants", 0);
		HideUnhideSelection("welliesboots", 1);
	}
	ref array<typename> m_Exceptionsjungle =
	{
		JungleBoots_ColorBase, MilitaryBoots_ColorBase, CombatBoots_ColorBase, TTSKOBoots, Loftd_IronsightBoots_ColorBase, Loftd_vagabondboots_ColorBase, Loftd_kronboots_ColorBase, Loftd_blacksuitboots
	};
	ref array<typename> m_Exceptionswellies =
	{
		Wellies_ColorBase, MedievalBoots, Kirza_Boots_ColorBase, High_Knee_Sneakers
	};
	//override void SetActions()
	//{
	//	super.SetActions();
	//	AddAction(ActionWringClothes);
	//}
	//void HideUnhideSelection(string selectionName, bool hide = false)
	//{
	//	TStringArray selectionNames = new TStringArray;
	//	ConfigGetTextArray("simpleHiddenSelections", selectionNames);
	//	int selectionId = selectionNames.Find(selectionName);
	//	SetSimpleHiddenSelectionState(selectionId, hide);
	//}
};

modded class Loftd_Sheriffpants_ColorBase extends Pants_Base
{
	override void EEInit()
	{
		super.EEInit();
		HideUnhideSelection("freepants", 0);
		HideUnhideSelection("welliesboots", 1);
	}
	ref array<typename> m_Exceptionswellies =
	{
		Wellies_ColorBase, MedievalBoots, Kirza_Boots_ColorBase, High_Knee_Sneakers
	};
	//override void SetActions()
	//{
	//	super.SetActions();
	//	AddAction(ActionWringClothes);
	//}
	//void HideUnhideSelection(string selectionName, bool hide = false)
	//{
	//	TStringArray selectionNames = new TStringArray;
	//	ConfigGetTextArray("simpleHiddenSelections", selectionNames);
	//	int selectionId = selectionNames.Find(selectionName);
	//	SetSimpleHiddenSelectionState(selectionId, hide);
	//}
};