modded class PlayerBase
{
	override void OnCommandSwimStart()
	{
		super.OnCommandSwimStart();
		
		if ( GetInventory() ) { GetInventory().UnlockInventory(LOCK_FROM_SCRIPT); }
		//if ( GetItemInHands() ) { GetItemAccessor().HideItemInHands(false); }
	}
	
	override void OnCommandSwimFinish()
	{
		if ( GetInventory() ) { GetInventory().LockInventory(LOCK_FROM_SCRIPT); }
		//if ( GetItemInHands() ) { GetItemAccessor().HideItemInHands(true); }
		
		super.OnCommandSwimFinish();
	}

	override void OnCommandVehicleStart()
	{
		super.OnCommandVehicleStart();
		
		if ( GetInventory() ) { GetInventory().UnlockInventory(LOCK_FROM_SCRIPT); }
	}
	
	override void OnCommandVehicleFinish()
	{
		if ( GetInventory() ) { GetInventory().LockInventory(LOCK_FROM_SCRIPT); }
		super.OnCommandVehicleFinish();
	}

	override bool CanReceiveItemIntoHands (EntityAI item_to_hands)
	{
		if( IsSwimming() || IsInVehicle() ) { return true; }
		return super.CanReceiveItemIntoHands(item_to_hands);
	}

	override bool CanReceiveAttachment(EntityAI attachment, int slotId)
	{
		ItemBase item = ItemBase.Cast(attachment);
		//Print("Slot:" + slot_name + " Item: " + attachment.GetType() + " Is Xbow:" + item.IsKindOf("Crossbow_Base"));

		if (slotId == InventorySlots.SHOULDER)
		{
			if (item.IsKindOf("Crossbow_Base") || item.IsKindOf("Loftd_weaponbelt_ColorBase"))
			{
				return true;
			}
			else
			{
				return false;
			}
		};

		if (slotId == InventorySlots.MELEE)
		{
			if (item.IsKindOf("Crossbow_Base") || item.IsKindOf("Loftd_meleebelt_ColorBase") || item.IsKindOf("meleesling_colorbase_mung"))
			{
				return true;
			}
			else
			{
				return false;
			}
		};

		return super.CanReceiveAttachment(attachment, slotId);
	}
}