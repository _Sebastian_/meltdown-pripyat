modded class ItemBase
{
	// returns item's protection level against enviromental hazard, for masks with filters, returns the filters protection for valid filter, otherwise 0
	override float GetProtectionLevel(int type, bool consider_filter = false, int system = 0)
	{
		if (IsDamageDestroyed() || (HasQuantity() && GetQuantity() <= 0) )
		{
			return 0;
		}
		
		if (GetInventory().HasAttachmentSlot(InventorySlots.BODY) || GetInventory().HasAttachmentSlot(InventorySlots.LEGS) || GetInventory().HasAttachmentSlot(InventorySlots.FEET))
		{
         //dont check the attachments of the NBC gear
		 //Print("DEBUG TYPE: "+ this.GetType() );
		}
		else
		{

			if( GetInventory().GetAttachmentSlotsCount() != 0 )//is it an item with attachable filter ?
			{
				ItemBase filter = ItemBase.Cast(FindAttachmentBySlotName("GasMaskFilter"));
				if (filter)
				{
					return filter.GetProtectionLevel(type, false, system);//it's a valid filter, return the protection
				}
				else 
				{
					return 0;//otherwise return 0 when no filter attached
				}	
			}
		}
		
		string subclass_path, entryName;

		switch (type)
		{
			case DEF_BIOLOGICAL:
				entryName = "biological";
				break;
			case DEF_CHEMICAL:
				entryName = "chemical";
				break;
			// case DEF_NUCLEAR:
			// 	entryName = "nuclear";
			// 	break;		
			default:
				entryName = "biological";
				break;
		}
		
		subclass_path = "CfgVehicles " + this.GetType() + " Protection ";
		float protection = GetGame().ConfigGetFloat(subclass_path + entryName);
		if (protection == 0)
		{
			protection = 1 - this.GetWetMax();
		}

		//Print("Chemical " + protection + " " + this.GetType());
		return protection;
	}

	override bool CanReceiveAttachment(EntityAI attachment, int slotId)
	{
		GameInventory attachmentInv = attachment.GetInventory();
		if (attachmentInv && attachmentInv.GetCargo() && attachmentInv.GetCargo().GetItemCount() > 0)
		{
			if (GetHierarchyParent() && !GetHierarchyParent().IsInherited(PlayerBase))
				return false;
		}

		if (slotId == InventorySlots.BODY || slotId == InventorySlots.LEGS || slotId == InventorySlots.FEET)
		{
			if (!this.IsKindOf("bl_cabinet"))
			{
				ItemBase item = ItemBase.Cast(attachment);
				if (item.GetInventory().HasAttachmentSlot(slotId))
				{
					return false;
				}
				if (this.IsKindOf("NBCBootsBase"))
				{
					int size_x, size_y;
					GetGame().GetInventoryItemSize(item, size_x, size_y);
					if (size_y > 2)
					{
						return false;
					}
				}
			}
		};

		return super.CanReceiveAttachment(attachment, slotId);
	}

	//override bool CanAssignToQuickbar()
	//{
	//	Clothing parent = Clothing.Cast(GetHierarchyParent());
	//	if (parent)
	//		return parent.CanAccess();

	//	return super.CanAssignToQuickbar();
	//}
};