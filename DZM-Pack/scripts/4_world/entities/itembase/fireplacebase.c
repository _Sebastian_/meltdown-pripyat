modded class FireplaceBase extends ItemBase
{
	protected float m_TemperatureLossMP = 0.25;		//mod 1.0 ... ! determines how fast will the fireplace loose its temperature when cooling (lower is better)
	protected float m_FuelBurnRateMP = 0.25;		//mod 1.0 ... ! determines how fast will the fuel item burn before spending (lower is better)

	const float	PARAM_TEMPERATURE_INCREASE = 1;		//mod 3 ... ! how much will temperature increase when fireplace is burning (degree Celsius per second)
	const float	PARAM_TEMPERATURE_DECREASE = 1;		//mod 3 ... ! how much will temperature decrease when fireplace is cooling (degree Celsius per second)
	const float	PARAM_FIRE_CONSUM_RATE_AMOUNT = 0.1;		//mod 0.5 ... ! base value of fire consumption rate (how many base energy will be spent on each update)
	const float	PARAM_BURN_DAMAGE_COEF = 1.0;		//mod 5.0 ! value for calculating damage on items located in fireplace cargo
	const float	PARAM_ITEM_HEAT_TEMP_INCREASE_COEF = 2.5;		//mod 10 ... ! value for calculating temperature increase on each heat update interval (degree Celsius)
	const float PARAM_COOKING_EQUIP_TEMP_INCREASE = 2.5;		//mod 10 ... ! how much will temperature increase when attached on burning fireplace (degree Celsius)
	const float	DIRECT_COOKING_SPEED = 0.375;		//mod 1.5 ... per second
	const float SMOKING_SPEED = 0.25;		//mod 1.5 ... per second
}