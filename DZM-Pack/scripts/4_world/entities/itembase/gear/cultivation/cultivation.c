modded class Plant_Cannabis : PlantBase {
	void Plant_Cannabis()
	{
		m_FullMaturityTime = 11520;//mod 1350 irl 56 days
	}
};
modded class Plant_Pepper : PlantBase
{
	void Plant_Pepper()
	{
		m_FullMaturityTime = 14400;//mod 2250 irl 70 days
	}
};
modded class Plant_Potato : PlantBase
{
	void Plant_Potato()
	{
		m_FullMaturityTime = 18720;//mod 2850 irl 91 days
	}
};
modded class Plant_Pumpkin : PlantBase
{
	void Plant_Pumpkin()
	{
		m_FullMaturityTime = 17280;//mod 2850 irl 84 days
	}
};
modded class Plant_Tomato : PlantBase
{
	void Plant_Tomato()
	{
		m_FullMaturityTime = 14400;//mod 1650 irl 70 days
	}
};
modded class Plant_Zucchini : PlantBase
{
	void Plant_Zucchini()
	{
		m_FullMaturityTime = 11520;//mod 1350 irl 56 days
	}
};