modded class Clothing
{
    bool CanAccess()
    {
        array<string> invSlots = new array<string>;
        ConfigGetTextArray("inventorySlot", invSlots);
        bool isBack = invSlots.Find("Back") != -1;
        bool isVest = invSlots.Find("Vest") != -1;

        bool canDo = true;

        if (isBack)
        {
            PlayerBase player = PlayerBase.Cast(GetHierarchyRootPlayer());
            if (player && player == GetGame().GetPlayer())
            {
                ItemBase back = player.GetItemOnSlot("BACK");
                if (back == this || back.GetInventory().HasAttachment(this))
                {
                    canDo = false;
                }
            }
        }

        Clothing parent = Clothing.Cast(GetHierarchyParent());
        if (parent && !isBack && !isVest)
        {
            canDo = false;
        }

        //if (parent && !parent.IsKindOf("carrierrig_mung"))
        //{
        //    canDo = false;
        //}

        //if (parent)
        //{
        //    Clothing parent2 = Clothing.Cast(parent.GetHierarchyParent());
        //    Clothing backItem = Clothing.Cast(back);
        //    if (parent2 && parent2.IsKindOf("carrierrig_mung") && parent2 == backItem)
        //    {
        //        canDo = false;
        //    }
        //}

        return canDo;
    }

    //override bool CanDisplayCargo()
    //{
    //    super.CanDisplayCargo();
    //    return CanAccess();
    //}

    override bool CanReceiveItemIntoCargo(EntityAI item)
    {
        super.CanReceiveItemIntoCargo(item);
        return CanAccess();
    }

    override bool CanReleaseCargo(EntityAI cargo)
    {
        super.CanReceiveItemIntoCargo(cargo);
        return CanAccess();
    }
};