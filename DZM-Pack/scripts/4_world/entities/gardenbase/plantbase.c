modded class PlantBase extends ItemBase
{
	private const float SPOIL_AFTER_MATURITY_TIME = 604800; //Mod 14400 ...The time it takes for a fully grown plant to spoil, in seconds

	override bool IsGrowing()
	{
		if (GetGame().GetMission().GetWorldData().GetBaseEnvTemperature() < 5)
		{
			return false;
		}
		if (GetPlantState() == STATE_GROWING)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	override void Harvest(PlayerBase player)
	{
		//TODO Boris: Add soft skill 2.0
		//PluginExperience module_exp = GetPlugin(PluginExperience);
		//float harvesting_efficiency = module_exp.GetExpParamNumber(player, PluginExperience.EXP_FARMER_HARVESTING, "efficiency");

		//m_CropsCount = m_CropsCount * harvesting_efficiency;

		if (!IsSpoiled())
		{
			for (int i = 0; i < m_CropsCount; i++)
			{
				vector pos = player.GetPosition();
				ItemBase item = ItemBase.Cast(GetGame().CreateObjectEx(m_CropsType, pos, ECE_PLACE_ON_SURFACE));
				item.SetQuantity(item.GetQuantityMax());
			}
			SetSpoiled();
		}

		m_HasCrops = false;
		SetSynchDirty();
		UpdatePlant();
	}
};