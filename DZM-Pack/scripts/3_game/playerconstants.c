modded class PlayerConstants 
{
	static const float BAREFOOT_MOVEMENT_BLEED_MODIFIER = 0.05;//mod 0.1
	static const float SHOES_MOVEMENT_DAMAGE_PER_STEP = 0.0035;//mod 0.035
	
	static const float SL_HEALTH_CRITICAL = 20;//mod 15
	static const float SL_HEALTH_LOW = 40;//mod 30
	static const float SL_HEALTH_NORMAL = 60;//mod 50
	static const float SL_HEALTH_HIGH = 80;//mod 80

	static const float SL_ENERGY_CRITICAL = 600;
	static const float SL_ENERGY_LOW = 1200;
	static const float SL_ENERGY_NORMAL = 1800;
	static const float SL_ENERGY_HIGH = 2400;
	static const float LOW_ENERGY_THRESHOLD = SL_ENERGY_CRITICAL;

	static const float SL_WATER_CRITICAL = 300;
	static const float SL_WATER_LOW = 600;
	static const float SL_WATER_NORMAL = 900;
	static const float SL_WATER_HIGH = 1200;
	static const float LOW_WATER_THRESHOLD = SL_WATER_CRITICAL;

	static const float DIGESTION_SPEED = 1;//mod 1.7	//quantity processed in the stomach per second
	//static const float CONSUMPTION_MULTIPLIER_BASE = 1;	//must not be 0 or less
	static const float STOMACH_ENERGY_TRANSFERED_PER_SEC = 1;//mod 3	//amount of kcal transfered to energy per second(this should ideally be higher than what player burns under high metabolic load[sprint])
	static const float STOMACH_WATER_TRANSFERED_PER_SEC = 2;//mod 6	//amount of ml transfered to water per second(this should ideally be higher than what player burns under high metabolic load[sprint])
	static const float STOMACH_SOLID_EMPTIED_PER_SEC = 2.5;//mod 7	//amount of g/ml emptied from stomach per second

	static const float METABOLIC_SPEED_ENERGY_BASAL		= 0.02;//mod 0.01		//energy loss per second while idle
	
	static const float METABOLIC_SPEED_ENERGY_WALK		= 0.05;//mod 0.1		//energy loss per second
	static const float METABOLIC_SPEED_ENERGY_JOG		= 0.2;//mod 0.3		//energy loss per second
	static const float METABOLIC_SPEED_ENERGY_SPRINT	= 0.8;//mod 0.6		//energy loss per second
	
	static const float METABOLIC_SPEED_WATER_BASAL		= 0.02;//mod 0.01		//water loss per second while idle
	
	static const float METABOLIC_SPEED_WATER_WALK		= 0.05;//mod 0.1		//water loss per second
	static const float METABOLIC_SPEED_WATER_JOG		= 0.2;//mod 0.3		//water loss per second
	static const float METABOLIC_SPEED_WATER_SPRINT		= 0.8;//mod 0.6		//water loss per second

	static const float BLOOD_REGEN_RATE_PER_SEC				= 0.025;//mod 0.3	//base amount of blood regenerated per second

	static const float WATER_LOSS_HC_PLUS_LOW				= 0.1;//mod 0
	static const float WATER_LOSS_HC_PLUS_HIGH				= 0.2;//mod 0.4

	static const float ENERGY_LOSS_HC_MINUS_LOW				= 0.1;//mod 0.2
	static const float ENERGY_LOSS_HC_MINUS_HIGH			= 0.2;//mod 0.45
	
	static const float HEALTH_LOSS_HC_PLUS_LOW				= 0.0125;//mod 0.05
	static const float HEALTH_LOSS_HC_PLUS_HIGH				= 0.025;//mod 0.15		
	
	static const float HEALTH_LOSS_HC_MINUS_LOW				= 0.0125;//mod 0.05
	static const float HEALTH_LOSS_HC_MINUS_HIGH 			= 0.025;//mod 0.15
	
	static const float WATER_LOSS_FEVER						= 0.05;//mod 0.2
	
	static const float LOW_ENERGY_DAMAGE_PER_SEC			= HEALTH_REGEN_MAX + 0.0005;//mod 0.05	//health loss per second while low on energy
	static const float LOW_WATER_DAMAGE_PER_SEC				= HEALTH_REGEN_MAX + 0.005;//mod 0.05	//health loss per second while low on water
	
	static const float HEALTH_REGEN_MIN						= 0.00125;//mod 0.005	//health regen rate at BLOOD_THRESHOLD_FATAL blood level
	static const float HEALTH_REGEN_MAX						= 0.005;//mod 0.03		//health regen rate at MAXIMUM blood level
	
	static const float LEG_HEALTH_REGEN						= 0.5;//mod 1		//Leg health regen when leg is NOT BROKEN
	static const float LEG_HEALTH_REGEN_BROKEN				= 0.03;//mod 0.18		//Leg health regen when BROKEN OR SPLINTED
	
	// static const float SHOCK_REFILL_CONSCIOUS_SPEED			= 4;//mod 5		//shock refill speed when the player is conscious
	// static const float SHOCK_REFILl_UNCONSCIOUS_SPEED		= 0.5;//mod 1		//shock refill speed when the player is unconscious
	
	static const float UNCONSCIOUS_IN_WATER_TIME_LIMIT_TO_DEATH	= 60;//mod 20	// how long can player survive while unconscious when in water in secs 

	static const float BLEEDING_SOURCE_BLOODLOSS_PER_SEC 	= -1;//mod -20 		// amount of blood loss per second from one bleeding source 

	static const float CHANCE_TO_BLEED_SLIDING_LADDER_PER_SEC = 0.1;//mod 0.3 // probability of bleeding source occuring while sliding down ladder without gloves given as percentage per second(0.5 means 50% chance bleeding source will happen every second while sliding down) 
	static const float GLOVES_DAMAGE_SLIDING_LADDER_PER_SEC = -0.1;//mod -3 // how much damage the gloves receive while sliding down the ladder (per sec)
	
	// static const float	BROKEN_LEGS_STAND_SHOCK = 2;//mod 0				//Shock dealt when standing with broken legs
	// static const float	BROKEN_CROUCH_MODIFIER = 0.75;//mod 0.5				//Inflicted shock modifier for crouched stance
}