modded class GameConstants
{
	// const int 	STAMINA_GAIN_JOG_PER_SEC = 1; //mod 2 ... in units (how much of stamina units is gained while jogging)
	const float ENVIRO_TEMPERATURE_HEIGHT_REDUCTION = 0.0065;		//mod 0.02 ... ! amount of ?C reduced for each meter of height above water level
	const float ENVIRO_TEMPERATURE_WIND_COEF = 1.0;		//mod 2.5 ... ! windchill effect on base temperature
	//const float ENVIRO_ISOLATION_WETFACTOR_WET = 0.7;//mod 0.75
	//const float ENVIRO_ISOLATION_WETFACTOR_SOAKED = 0.3;//mod 0.5
	//const float ENVIRO_ISOLATION_HEALTHFACTOR_DAMAGED = 0.7;//mod 0.8
	//const float ENVIRO_ISOLATION_HEALTHFACTOR_B_DAMAGED = 0.3;//mod 0.5

	const float STAMINA_JUMP_THRESHOLD = STAMINA_DRAIN_JUMP; // in units
	const float STAMINA_VAULT_THRESHOLD = STAMINA_DRAIN_VAULT; // in units
	const float STAMINA_CLIMB_THRESHOLD = STAMINA_DRAIN_CLIMB; // in units
	const float STAMINA_REGEN_COOLDOWN_DEPLETION = 3; // in secs (how much time we will spend in cooldown before the stamina will starts with regeneration)
	const float STAMINA_REGEN_COOLDOWN_EXHAUSTION = 5;
	const float STAMINA_SYNC_RATE = 1; //in secs

	const float FIRE_ATTACHMENT_DAMAGE_PER_SECOND = 0.0007;//0.07		//! damage per second dealt to attachment by fire

	const float DECAY_FOOD_RAW_MEAT = 86400;//21600
	const float DECAY_FOOD_RAW_CORPSE = 129600;//32400
	const float DECAY_FOOD_RAW_FRVG = 172800;//43200
	const float DECAY_FOOD_BOILED_MEAT = 1036800;//259200
	const float DECAY_FOOD_BOILED_FRVG = 691200;//172800
	const float DECAY_FOOD_BAKED_MEAT = 1382400;//345600
	const float DECAY_FOOD_BAKED_FRVG = 1036800;//259200
	const float DECAY_FOOD_DRIED_MEAT = 2764800;//691200
	const float DECAY_FOOD_CAN_OPEN = 691200;//172800
	const float DECAY_RATE_ON_PLAYER = 1.25;//2.5
}