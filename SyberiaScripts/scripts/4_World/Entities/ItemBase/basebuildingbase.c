//BASE BUILDING BASE
modded class BaseBuildingBase extends ItemBase
{
		
	//folding
	override bool CanFoldBaseBuildingObject()
	{
		if ( HasBase() || !IsEmpty())
		{
			return false;
		}
		
		return true;
	}
}