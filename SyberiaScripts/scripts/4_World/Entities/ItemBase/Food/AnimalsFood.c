modded class DeadChicken_ColorBase
{
	override bool CanBeSkinned()
	{
		if (GetHealthLevel() == GameConstants.STATE_RUINED)
			return false;

		return true;
	}
	override bool IsSkinned()
	{
		return !CanBeSkinned();
	}
	override bool IsAlive()
	{
		return false;
	}
};

modded class DeadRabbit
{
	override bool CanBeSkinned()
	{
		if (GetHealthLevel() == GameConstants.STATE_RUINED)
			return false;

		return true;
	}
	override bool IsSkinned()
	{
		return !CanBeSkinned();
	}
	override bool IsAlive()
	{
		return false;
	}
};

modded class DeadFox
{
	override bool CanBeSkinned()
	{
		if (GetHealthLevel() == GameConstants.STATE_RUINED)
			return false;

		return true;
	}
	override bool IsSkinned()
	{
		return !CanBeSkinned();
	}
	override bool IsAlive()
	{
		return false;
	}
};

modded class Carp
{
	override bool CanBeSkinned()
	{
		if (GetHealthLevel() == GameConstants.STATE_RUINED)
			return false;

		return true;
	}
	override bool IsSkinned()
	{
		return !CanBeSkinned();
	}
	override bool IsAlive()
	{
		return false;
	}
}

modded class Mackerel
{
	override bool CanBeSkinned()
	{
		if (GetHealthLevel() == GameConstants.STATE_RUINED)
			return false;

		return true;
	}
	override bool IsSkinned()
	{
		return !CanBeSkinned();
	}
	override bool IsAlive()
	{
		return false;
	}
}

////zeroy
//modded class zeroy_cod
//{
//	override bool CanBeSkinned()
//	{
//		if (GetHealthLevel() == GameConstants.STATE_RUINED)
//			return false;
//
//		return true;
//	}
//	override bool IsSkinned()
//	{
//		return !CanBeSkinned();
//	}
//	override bool IsAlive()
//	{
//		return false;
//	}
//}
//
//modded class zeroy_pike
//{
//	override bool CanBeSkinned()
//	{
//		if (GetHealthLevel() == GameConstants.STATE_RUINED)
//			return false;
//
//		return true;
//	}
//	override bool IsSkinned()
//	{
//		return !CanBeSkinned();
//	}
//	override bool IsAlive()
//	{
//		return false;
//	}
//}
//
//modded class zeroy_salmon
//{
//	override bool CanBeSkinned()
//	{
//		if (GetHealthLevel() == GameConstants.STATE_RUINED)
//			return false;
//
//		return true;
//	}
//	override bool IsSkinned()
//	{
//		return !CanBeSkinned();
//	}
//	override bool IsAlive()
//	{
//		return false;
//	}
//}

//hunterz
//modded class DeadRat_ColorBase
//{
//	override bool CanBeSkinned()
//	{
//		if (GetHealthLevel() == GameConstants.STATE_RUINED)
//			return false;
//
//		return true;
//	}
//	override bool IsSkinned()
//	{
//		return !CanBeSkinned();
//	}
//	override bool IsAlive()
//	{
//		return false;
//	}
//};
//
//modded class DeadSquirrel_ColorBase
//{
//	override bool CanBeSkinned()
//	{
//		if (GetHealthLevel() == GameConstants.STATE_RUINED)
//			return false;
//
//		return true;
//	}
//	override bool IsSkinned()
//	{
//		return !CanBeSkinned();
//	}
//	override bool IsAlive()
//	{
//		return false;
//	}
//};

//modded class Snake_Base
//{
//	override bool CanBeSkinned()
//	{
//		if (GetHealthLevel() == GameConstants.STATE_RUINED)
//			return false;
//
//		return true;
//	}
//	override bool IsSkinned()
//	{
//		return !CanBeSkinned();
//	}
//	override bool IsAlive()
//	{
//		return false;
//	}
//};
