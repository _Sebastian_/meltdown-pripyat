class AuthSystem
{
    allowedLettersInName[] = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
};

class SleepingSystem
{
    // Client
    sleepingMaxValue = 20000;
    sleepingLevel5 = 4000;
    sleepingLevel4 = 8000;
    sleepingLevel3 = 12000;
    sleepingLevel2 = 16000;
};

class MindstateSystem
{
    mindstateMaxValue = 100;
    mindstateLevel5 = 20.0;
    mindstateLevel4 = 40.0;
    mindstateLevel3 = 60.0;
    mindstateLevel2 = 80.0;
};