class DZ_LightAI;
class AnimalBase : DZ_LightAI
{
	storageCategory = 3;
	knifeDamageModifier = 1;
	skinTimeModifier = 1;
};

class Animal_GallusGallusDomesticus : AnimalBase
{
	knifeDamageModifier = 0.25;
};

class Animal_GallusGallusDomesticusF : AnimalBase
{
	knifeDamageModifier = 0.25;
};

class Animal_BosTaurus : AnimalBase
{
	knifeDamageModifier = 1.5;
};

class Animal_BosTaurusF : AnimalBase
{
	knifeDamageModifier = 1.5;
};

class Animal_UrsusArctos : AnimalBase
{
	knifeDamageModifier = 2.0;
};

class Animal_VulpesVulpes : AnimalBase
{
	knifeDamageModifier = 0.5;
};

class DeadChicken_ColorBase : Edible_Base
{
	storageCategory = 3;
	knifeDamageModifier = 0.25;
	skinTimeModifier = 0.5;
	class Skinning
	{
		class ObtainedSteaks
		{
			item = "ChickenBreastMeat";
			count = 2;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
		class ObtainedFeathers
		{
			item = "ChickenFeather";
			count = 2;
			quantityMinMaxCoef[] = { 0.5,1 };
			transferToolDamageCoef = 1;
		};
		class ObtainedGuts
		{
			item = "SmallGuts";
			count = 1;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
		class ObtainedBones
		{
			item = "Bone";
			count = 1;
			quantityMinMaxCoef[] = { 0.2,0.3 };
			transferToolDamageCoef = 1;
		};
	};
};

class DeadRabbit : Edible_Base
{
	storageCategory = 3;
	knifeDamageModifier = 0.25;
	skinTimeModifier = 0.5;
	class Skinning
	{
		class ObtainedSteaks
		{
			item = "RabbitLegMeat";
			count = 2;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
		class ObtainedPelt
		{
			item = "RabbitPelt";
			count = 1;
			quantityCoef = 1;
			transferToolDamageCoef = 1;
		};
		class ObtainedGuts
		{
			item = "SmallGuts";
			count = 2;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
		class ObtainedBones
		{
			item = "Bone";
			count = 1;
			quantityMinMaxCoef[] = { 0.3,0.4 };
			transferToolDamageCoef = 1;
		};
	};
};

class DeadFox : Edible_Base
{
	storageCategory = 3;
	knifeDamageModifier = 0.5;
	skinTimeModifier = 0.5;
	class Skinning
	{
		class ObtainedSteaks
		{
			item = "FoxSteakMeat";
			count = 4;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
		class ObtainedPelt
		{
			item = "FoxPelt";
			count = 1;
			quantityCoef = 1;
			transferToolDamageCoef = 1;
		};
		class ObtainedGuts
		{
			item = "SmallGuts";
			count = 2;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
		class ObtainedLard
		{
			item = "Lard";
			count = 1;
			quantityMinMaxCoef[] = { 0.2,0.4 };
		};
		class ObtainedBones
		{
			item = "Bone";
			count = 1;
			quantityMinMaxCoef[] = { 0.4, 0.5 };
			transferToolDamageCoef = 1;
		};
	};
};

class Carp : Edible_Base
{
	storageCategory = 3;
	knifeDamageModifier = 0.25;
	skinTimeModifier = 0.25;
	class Skinning
	{
		class ObtainedSteaks
		{
			item = "CarpFilletMeat";
			count = 2;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
		class ObtainedGuts
		{
			item = "SmallGuts";
			count = 1;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
		class ObtainedLard
		{
			item = "RedCaviar";
			count = 1;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
	};
};

class Mackerel : Edible_Base
{
	storageCategory = 3;
	knifeDamageModifier = 0.25;
	skinTimeModifier = 0.25;
	class Skinning
	{
		class ObtainedSteaks
		{
			item = "MackerelFilletMeat";
			count = 2;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
		class ObtainedGuts
		{
			item = "SmallGuts";
			count = 1;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
		class ObtainedLard
		{
			item = "RedCaviar";
			count = 1;
			quantityMinMaxCoef[] = { 0.5,1 };
		};
	};
};

////zeroy
//class zeroy_cod : Edible_Base
//{
//	storageCategory = 3;
//	knifeDamageModifier = 0.375;
//	skinTimeModifier = 0.375;
//	class Skinning
//	{
//		class ObtainedSteaks
//		{
//			item = "CodFilletMeat";
//			count = 3;
//			quantityMinMaxCoef[] = { 0.5,1 };
//		};
//		class ObtainedGuts
//		{
//			item = "SmallGuts";
//			count = 1;
//			quantityMinMaxCoef[] = { 0.5,0.8 };
//		};
//	};
//};
//
//class zeroy_pike : Edible_Base
//{
//	storageCategory = 3;
//	knifeDamageModifier = 0.375;
//	skinTimeModifier = 0.375;
//	class Skinning
//	{
//		class ObtainedSteaks
//		{
//			item = "PikeFilletMeat";
//			count = 3;
//			quantityMinMaxCoef[] = { 0.5,1 };
//		};
//		class ObtainedGuts
//		{
//			item = "SmallGuts";
//			count = 1;
//			quantityMinMaxCoef[] = { 0.5,0.8 };
//		};
//	};
//};
//
//class zeroy_salmon : Edible_Base
//{
//	storageCategory = 3;
//	knifeDamageModifier = 0.5;
//	skinTimeModifier = 0.5;
//	class Skinning
//	{
//		class ObtainedSteaks
//		{
//			item = "SalmonFilletMeat";
//			count = 4;
//			quantityMinMaxCoef[] = { 0.5,1 };
//		};
//		class ObtainedGuts
//		{
//			item = "SmallGuts";
//			count = 2;
//			quantityMinMaxCoef[] = { 0.5,0.8 };
//		};
//	};
//};
//
////hunterz
//class DeadRat_ColorBase : Edible_Base
//{
//	storageCategory = 3;
//	knifeDamageModifier = 0.125;
//	skinTimeModifier = 0.25;
//	//class Skinning
//	//{
//	//	class ObtainedSteaks
//	//	{
//	//		item = "SkinnedRat";
//	//		count = 1;
//	//		quantityMinMaxCoef[] = { 0.5,1 };
//	//	};
//	//	class ObtainedGuts
//	//	{
//	//		item = "SmallGuts";
//	//		count = 1;
//	//		quantityMinMaxCoef[] = { 0.5,0.8 };
//	//	};
//	//	class ObtainedBones
//	//	{
//	//		item = "Bone";
//	//		count = 1;
//	//		quantityMinMaxCoef[] = { 0.1,0.2 };
//	//		transferToolDamageCoef = 1;
//	//	};
//	//};
//};
//
//class DeadSquirrel_ColorBase : Edible_Base
//{
//	storageCategory = 3;
//	knifeDamageModifier = 0.125;
//	skinTimeModifier = 0.25;
//	//class Skinning
//	//{
//	//	class ObtainedSteaks
//	//	{
//	//		item = "SkinnedSquirrel";
//	//		count = 1;
//	//		quantityMinMaxCoef[] = { 0.5,1 };
//	//	};
//	//	class ObtainedGuts
//	//	{
//	//		item = "SmallGuts";
//	//		count = 1;
//	//		quantityMinMaxCoef[] = { 0.5,0.8 };
//	//	};
//	//	class ObtainedBones
//	//	{
//	//		item = "Bone";
//	//		count = 1;
//	//		quantityMinMaxCoef[] = { 0.1,0.2 };
//	//		transferToolDamageCoef = 1;
//	//	};
//	//};
//};
//
//class Raven_Base : AnimalBase
//{
//	knifeDamageModifier = 0.25;
//	skinTimeModifier = 0.5;
//};
//
//class Seagull_Base : AnimalBase
//{
//	knifeDamageModifier = 0.25;
//	skinTimeModifier = 0.5;
//};
//
//class Otter_Base : AnimalBase
//{
//	knifeDamageModifier = 0.5;
//};
//
//class Rabbit_Base : AnimalBase
//{
//	knifeDamageModifier = 0.25;
//	skinTimeModifier = 0.5;
//};