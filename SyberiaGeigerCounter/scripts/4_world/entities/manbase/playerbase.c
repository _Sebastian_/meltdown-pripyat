modded class PlayerBase
{
	int GetRadLvl()
	{
		int maxLvl = 0;
		foreach (ref ZoneImplementation zoneImpl : m_zones)
		{
			if (zoneImpl.m_zone.m_radiation > maxLvl)
			{
				maxLvl = zoneImpl.m_zone.m_radiation;
			}
		}
		return maxLvl;
	}
}