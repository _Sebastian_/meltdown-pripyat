class GeigerBase extends ItemBase
{
    protected ref Timer m_GeigerSimTimer;

    override void InitItemVariables()
    {
        super.InitItemVariables();

        if ( GetGame().IsClient() || !GetGame().IsMultiplayer() )
        {
            m_GeigerSimTimer = new Timer( CALL_CATEGORY_GAMEPLAY );
        }
    }

    private void GeigerSimulation()
    {
        PlayerBase player = PlayerBase.Cast( GetHierarchyRootPlayer() );
        if (player)
        {
            int radLvl = player.GetRadLvl();
            OnGeigerSimulation(radLvl > 0);

            m_GeigerSimTimer.Run(1.0 / (float)radLvl, this, "GeigerSimulation", NULL, false);
        }
    }

    void PlayClick()
    {
        EffectSound clickSound;
        this.PlaySoundSet( clickSound, "GeigerCounterSoundSet", 0, 0 );
        clickSound.SetSoundAutodestroy( true );
    }

    protected void OnGeigerSimulation( bool detected_radiation )
    {
    }

    override void OnWorkStart()
    {
        super.OnWorkStart();

        if ( m_GeigerSimTimer && ( GetGame().IsClient() || !GetGame().IsMultiplayer() ) )
        {
            m_GeigerSimTimer.Run( 1.0, this, "GeigerSimulation", NULL, false );
        }
    }

    override void OnWorkStop()
    {
        super.OnWorkStop();

        if ( m_GeigerSimTimer && ( GetGame().IsClient() || !GetGame().IsMultiplayer() ) )
        {
            m_GeigerSimTimer.Stop();
        }
    }

    override void SetActions()
    {
        super.SetActions();

        AddAction( ActionTurnOffWhileInHands );
        AddAction( ActionTurnOnWhileInHands );
    }
}