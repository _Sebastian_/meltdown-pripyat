class Syb_Geigercounter extends GeigerBase
{
    override void OnGeigerSimulation( bool detected_radiation )
    {
        if ( detected_radiation)
        {
            PlayClick();
        }
    }

    override void OnWorkStart()
    {
        super.OnWorkStart();

    }

    override void OnWorkStop()
    {
        super.OnWorkStop();

    }
}