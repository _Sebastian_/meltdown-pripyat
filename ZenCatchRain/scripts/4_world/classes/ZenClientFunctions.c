class ZenClientFunctions
{
	// Is it currently raining heavily and overcast?
	static float GetRain()
	{
		return GetGame().GetWeather().GetRain().GetActual();
	}

	// Client-side only. Gets camera angle.
	static int GetCameraPitch()
	{
		if (!GetGame())
			return 0;

		PlayerBase player = PlayerBase.Cast(GetGame().GetPlayer());
		float pitch = 0;

		if (player)
		{
			DayZPlayerCamera camera = player.GetCurrentCamera();

			if (camera)
				pitch = camera.GetCurrentPitch();
		}

		return pitch;
	}
};